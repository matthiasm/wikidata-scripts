#!/usr/bin/python3

import urllib.request
import re
import pywikibot
from pywikibot.data.sparql import SparqlQuery
from bs4 import BeautifulSoup


repo = pywikibot.Site()
repo.login()


class DatabaseEntry():

    def __init__(self, gamingonlinux_id):
        url = f"https://gamingonlinux.com/itemdb/{str(gamingonlinux_id)}/"
        print(f"Processing URL {url}")
        try:
            request = urllib.request.Request(url, headers={"User-Agent": "Wikidata Import"})
            response = urllib.request.urlopen(request)
            html = response.read().decode("utf-8")
        except Exception as error:
            pywikibot.output(error)
            return

        self.html = html

    def get_steam_id(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        a = soup.find('a', string='Steam')
        if a is not None:
            match = re.match(r'^https:\/\/(?:store\.)?steampowered\.com\/app\/(\d+)', a['href'])
            if match:
                return f"\"{match.group(1)}\""

    def get_gog_id(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        a = soup.find('a', string='GOG')
        if a is not None:
            match = re.search(r'https:\/\/www\.gog\.com\/game\/([a-z\d]+(?:[\_]{1,2}[a-z\d]+){0,10})', a['href'])
            if match:
                return f"\"{match.group(1)}\""

    def get_official_website(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        a = soup.find('a', string='Official Site')
        if a is not None:
            return f"<{a['href']}>"

    def get_itch_url(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        a = soup.find('a', string='itch.io')
        if a is not None:
            return f"<{a['href']}>"


def find_item_from_external_id(property, id):
    sparql = SparqlQuery()
    try:
        results = sparql.select(f"""
            SELECT ?item WHERE {{
            ?item wdt:{property} {id} .
            }}
        """)
    except Exception as error:
        pywikibot.error("Failed to query for {id}", error)
        return None

    if len(results) == 0:
        return None
    match = re.match(r"^https?://www\.wikidata\.org/entity/(Q\d+)$", results[0]["item"])
    if not match:
        return None
    return pywikibot.ItemPage(repo, match.group(1))


def database_claim(gamingonlinux_id, external_id, wikidata_property, identifier_name):
    if external_id is None:
        return None

    wikidata_game = find_item_from_external_id(wikidata_property, external_id)
    if wikidata_game is not None:
        print(f"Found matching Wikidata {wikidata_game}")

        claims = wikidata_game.get(u'claims')
        if u'P12927' in claims[u'claims']:
            print(u'Skipping: Already has a GamingOnLinux ID!')
        else:
            claim = pywikibot.Claim(repo, u'P12927')
            claim.setTarget(str(gamingonlinux_id))
            wikidata_game.addClaim(claim, summary=u'based on ' + identifier_name)
            print("Adding GamingOnLinux ID.")

        print()
        return wikidata_game

    return None


if __name__ == "__main__":
    for gamingonlinux_id in range(1, 39511):
        gamingonlinux_entry = DatabaseEntry(gamingonlinux_id)

        wikidata_game = database_claim(gamingonlinux_id, gamingonlinux_entry.get_steam_id(), 'P1733', 'Steam application ID')
        if wikidata_game is not None:
            continue

        wikidata_game = database_claim(gamingonlinux_id, gamingonlinux_entry.get_gog_id(), 'P2725', 'GOG application ID')
        if wikidata_game is not None:
            continue

        wikidata_game = database_claim(gamingonlinux_id, gamingonlinux_entry.get_itch_url(), 'P7294', 'itch.io URL')
        if wikidata_game is not None:
            continue

        wikidata_game = database_claim(gamingonlinux_id, gamingonlinux_entry.get_official_website(), 'P856', 'official website')
        if wikidata_game is not None:
            continue

        if wikidata_game is None:
            print("No match in Wikidata.")
            print()
