#!/usr/bin/python3

import re
import pywikibot
import urllib.request
from pywikibot.data.sparql import SparqlQuery
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager

repo = pywikibot.Site()
repo.login()

options = Options()
options.add_argument("--headless")

driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()), options=options)

class GameFAQsEntry():

    def __init__(self, gamefaqs_id):
        url = f"https://gamefaqs.gamespot.com/p/{str(gamefaqs_id)}-"
        pywikibot.output(f"Processing URL {url}")
        try:
            driver.get(url)
            self.html = driver.page_source
        except Exception as error:
            pywikibot.output(error)
            return

    def get_metacritic_id(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        div = soup.find('div', class_='metacritic')
        if div:
            a = div.find('a', href=lambda x: x and 'metacritic.com' in x)
            if a and 'href' in a.attrs:
                return a['href'].split('/')[-1].split('?')[0]


class SteamEntry():

    def __init__(self, steam_id):
        url = f"https://store.steampowered.com/app/{str(steam_id)}"
        pywikibot.output(f"Processing URL {url}")
        try:
            request = urllib.request.Request(url, None)
            response = urllib.request.urlopen(request)
            self.html = response.read().decode("utf-8")
        except Exception as error:
            pywikibot.output(error)
            return

    def get_metacritic_id(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        div = soup.find('div', id='game_area_metalink')
        if div:
            a = div.find('a', href=lambda x: x and 'metacritic.com' in x)
            if a and 'href' in a.attrs:
                return a['href'].split('/')[-1].split('?')[0]


def find_item_from_external_id(property, id):
    sparql = SparqlQuery()
    try:
        results = sparql.select(f"""
            SELECT ?item WHERE {{
            ?item wdt:{property} "{id}" .
            }}
        """)
    except Exception as error:
        pywikibot.error("Failed to query for {id}", error)
        return None

    if len(results) == 0:
        return None
    match = re.match(r"^https?://www\.wikidata\.org/entity/(Q\d+)$", results[0]["item"])
    if not match:
        return None
    return pywikibot.ItemPage(repo, match.group(1))


def harvest_gamefaqs():
    for gamefaqs_id in range(100000, 469450):
        gamefaqs_entry = GameFAQsEntry(gamefaqs_id)
        metacritic_game_id = gamefaqs_entry.get_metacritic_id()
        if not metacritic_game_id is None:
            wikidata_game = find_item_from_external_id('P4769', gamefaqs_id)
            if wikidata_game is not None:
                print(f"Found matching Wikidata {wikidata_game}")

                claims = wikidata_game.get(u'claims')
                if u'P12054' in claims[u'claims']:
                    print(u'Skipping: Already has a Metacritic game ID!')
                else:
                    metacritic_game_claim = pywikibot.Claim(repo, u'P12054')
                    metacritic_game_claim.setTarget(str(metacritic_game_id))
                    wikidata_game.addClaim(metacritic_game_claim, summary=u'based on GameFAQs game ID.')
                    print(f"Adding Metacritic game ID.")

                print()
                continue
            else:
                print(f"No match in Wikidata.")

        print()


def harvest_steam():
    for steam_id in range(766320, 2698780, 10):
        steam_entry = SteamEntry(steam_id)
        metacritic_game_id = steam_entry.get_metacritic_id()
        if not metacritic_game_id is None:
            wikidata_game = find_item_from_external_id('P1733', steam_id)
            if wikidata_game is not None:
                print(f"Found matching Wikidata {wikidata_game}")

                claims = wikidata_game.get(u'claims')
                if u'P12054' in claims[u'claims']:
                    print(u'Skipping: Already has a Metacritic game ID!')
                else:
                    metacritic_game_claim = pywikibot.Claim(repo, u'P12054')
                    metacritic_game_claim.setTarget(str(metacritic_game_id))
                    wikidata_game.addClaim(metacritic_game_claim, summary=u'based on Steam application ID.')
                    print(f"Adding Metacritic game ID.")

                print()
                continue
            else:
                print(f"No match in Wikidata.")

        print()


if __name__ == "__main__":
    #harvest_gamefaqs()
    harvest_steam()