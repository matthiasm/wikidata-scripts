#!/usr/bin/env python

from pywikibot.data.sparql import SparqlQuery
import pywikibot
import requests
import string
import re

repo = pywikibot.Site()
repo.login()

def find_item_from_external_id(property, id):
    sparql = SparqlQuery()
    try:
        results = sparql.select(f"""
            SELECT ?item WHERE {{
            ?item wdt:{property} {id} .
            }}
        """)
    except Exception as error:
        pywikibot.error("Failed to query for {id}", error)
        return None

    if len(results) == 0:
        return None
    match = re.match(r"^https?://www\.wikidata\.org/entity/(Q\d+)$", results[0]["item"])
    if not match:
        return None
    return pywikibot.ItemPage(repo, match.group(1))


def match_python():
    for letter in string.ascii_lowercase:
        url = f"https://archlinux.org/packages/search/json/?q=python-{letter}"
        response = requests.get(url)
        data = response.json()
        results = data.get('results')
        for result in results:
            package = result.get('pkgname')
            if not package.startswith('python-'):
                provides = result.get('provides')
                for provide in provides:
                    if provide.startswith('python-'):
                        package = provide

            if not package.startswith('python-'):
                continue

            print(package)

            entity = find_item_from_external_id('P5568', f"\"{package.split('python-')[1]}\"")
            if entity is not None:
                print(f"Found matching Wikidata {entity}")
                claims = entity.get(u'claims')
                if u'P3454' in claims[u'claims']:
                    print(u'Skipping: Already has an Arch Linux package!')
                else:
                    try:
                        print(f"Adding Arch Linux package")
                        claim = pywikibot.Claim(repo, u'P3454')
                        claim.setTarget(package)
                        entity.addClaim(claim, summary=u'based on package name.')
                    except Exception as error:
                        pywikibot.error("Failed add claim for {package}", error)
            print()

def match_perl():
    for letter in string.ascii_lowercase:
        url = f"https://archlinux.org/packages/search/json/?q=perl-{letter}"
        response = requests.get(url)
        data = response.json()
        results = data.get('results')
        for result in results:
            package = result.get('pkgname')
            print(f"Arch Linux package {package}")

            url = result.get('url')
            if url is None:
                continue

            if url.startswith('https://metacpan.org/release/'):
                entity = find_item_from_external_id('P5779', f"\"{url.split('https://metacpan.org/release/')[1]}\"")
            elif url.startswith('https://search.cpan.org/dist/'):
                entity = find_item_from_external_id('P5779', f"\"{url.split('https://search.cpan.org/dist/')[1]}\"")

            if entity is not None:
                print(f"Found matching Wikidata {entity}")
                claims = entity.get(u'claims')
                if u'P3454' in claims[u'claims']:
                    print(u'Skipping: Already has an Arch Linux package!')
                else:
                    try:
                        print(f"Adding Arch Linux package")
                        claim = pywikibot.Claim(repo, u'P3454')
                        claim.setTarget(package)
                        entity.addClaim(claim, summary=u'based on URL.')
                    except Exception as error:
                        pywikibot.error("Failed add claim for {package}", error)
                print()

#match_python()
match_perl()
