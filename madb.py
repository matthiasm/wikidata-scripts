#!/usr/bin/python3

import pywikibot
import urllib.request
from bs4 import BeautifulSoup
import re
from pywikibot.data.sparql import SparqlQuery

repo = pywikibot.Site()
repo.login()


def get_packages(page):
    url = f"http://madb.mageia.org/package/list/page/{str(page)}"
    print(f"Processing URL {url}")
    try:
        request = urllib.request.Request(url, None)
        response = urllib.request.urlopen(request)
        html = response.read().decode("utf-8")
    except Exception as error:
        pywikibot.output(error)

    soup = BeautifulSoup(html, 'lxml')
    ul = soup.find('ul', class_='packlist')
    for a in ul.findAll('a'):
        yield a.get_text()


def get_website_url(package):
    url = f"https://madb.mageia.org/package/show/name/{package}"
    print(f"Processing URL {url}")
    try:
        request = urllib.request.Request(url, None)
        response = urllib.request.urlopen(request)
        html_reponse = response.read().decode("utf-8")
    except Exception as error:
        pywikibot.stdout(error)
        return None

    try:
        soup = BeautifulSoup(html_reponse, 'lxml')
        return soup.find('strong', string='URL').find_next_sibling('a')['href']
    except Exception as error:
        pywikibot.stdout(error)
        return None


def find_item_from_official_website(url):
    pywikibot.output(f"Searching website {url}")
    sparql = SparqlQuery()
    try:
        result = sparql.select(f"""
            SELECT ?item WHERE {{
                ?item wdt:P856 <{url}> .
            }}
        """)
    except Exception as error:
        pywikibot.output(error)
        return None
    if result is None:
        return None
    if len(result) != 1:
        return None
    match = re.match(r"^https?://www\.wikidata\.org/entity/(Q\d+)$", result[0]["item"])
    if not match:
        return None
    return pywikibot.ItemPage(repo, match.group(1))


def extract_sourceforge_project(url):
    if url is None:
        return

    match = re.search(r'sourceforge\.net/p/([^/]+)', url)
    if match:
        return match.group(1)

    match = re.search(r'http://([^\.]+)\.sourceforge\.net', url)
    if match:
        return match.group(1)

    match = re.search(r'sourceforge\.net/projects/([^/]+)', url)
    if match:
        return match.group(1)


def find_item_from_sourceforge_project(item):
    if item is None:
        return None

    pywikibot.output(f"Searching SourceForge {item}")
    sparql = SparqlQuery()
    try:
        result = sparql.select(f"""
            SELECT ?item WHERE {{
                ?item wdt:P2209 <{item}> .
            }}
        """)
    except Exception as error:
        pywikibot.output(error)
        return None
    if result is None:
        return None
    if len(result) != 1:
        return None
    match = re.match(r"^https?://www\.wikidata\.org/entity/(Q\d+)$", result[0]["item"])
    if not match:
        return None
    return pywikibot.ItemPage(repo, match.group(1))


def extract_launchpad_id(url):
    if url is None:
        return None

    match = re.search(r'launchpad\.net/([^/]+)', url)
    if match:
        return match.group(1)
    return None


def find_item_from_launchpad_project(item):
    pywikibot.output(f"Searching Launchpad {item}")
    sparql = SparqlQuery()
    try:
        result = sparql.select(f"""
            SELECT ?item WHERE {{
                ?item wdt:P3802 <{item}> .
            }}
        """)
    except Exception as error:
        pywikibot.output(error)
        return None
    if result is None:
        return None
    if len(result) != 1:
        return None
    match = re.match(r"^https?://www\.wikidata\.org/entity/(Q\d+)$", result[0]["item"])
    if not match:
        return None
    return pywikibot.ItemPage(repo, match.group(1))


def find_item_from_source_code_repo(url):
    pywikibot.output(f"Searching Repository {url}")
    sparql = SparqlQuery()
    try:
        result = sparql.select(f"""
            SELECT ?item WHERE {{
                ?item wdt:P1324 <{url}> .
            }}
        """)
    except Exception as error:
        pywikibot.output(error)
        return None
    if result is None:
        return None
    if len(result) != 1:
        return None
    match = re.match(r"^https?://www\.wikidata\.org/entity/(Q\d+)$", result[0]["item"])
    if not match:
        return None
    return pywikibot.ItemPage(repo, match.group(1))


for page in range(1, 42):
    for package in get_packages(page):
        url = get_website_url(package)
        entity = find_item_from_source_code_repo(url)
        if entity is None:
            entity = find_item_from_sourceforge_project(extract_sourceforge_project(url))
        if entity is None:
            entity = find_item_from_launchpad_project(extract_launchpad_id(url))
        if entity is None:
            entity = find_item_from_source_code_repo(url)
        if entity is None:
            entity = find_item_from_official_website(url)
        if entity is not None:
            print(f"Found matching Wikidata {entity}")
            claims = entity.get(u'claims')
            if u'P12030' in claims[u'claims']:
                print(u'Skipping: Already has a Mageia package!')
            else:
                claim = pywikibot.Claim(repo, u'P12030')
                claim.setTarget(package)
                entity.addClaim(claim, summary=u'Add Mageia package based on URL.')
                print(f"Adding Mageia package")
        print()
