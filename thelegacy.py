#!/usr/bin/python3

import re
import time
import pywikibot
import urllib.request
from pywikibot.data.sparql import SparqlQuery
from bs4 import BeautifulSoup

site = pywikibot.Site(url='https://www.c64-wiki.de', fam='c64')

repo = pywikibot.Site()
repo.login()


def find_item_from_external_id(property, id):
    sparql = SparqlQuery()
    try:
        results = sparql.select(f"""
            SELECT ?item WHERE {{
            ?item wdt:{property} "{id}" .
            }}
        """)
    except Exception as error:
        pywikibot.error("Failed to query for {id}", error)
        return None

    if results is None:
        return None

    if len(results) == 0:
        return None
    match = re.match(r"^https?://www\.wikidata\.org/entity/(Q\d+)$", results[0]["item"])
    if not match:
        return None
    return pywikibot.ItemPage(repo, match.group(1))


def extract_template_id(source_content):
    match = re.search(r"{{thelegacy\|(\d+)}}", source_content)
    if match:
        return match.group(1)
    else:
        return None


def claim_legacy(wikidata_game, property, id, base):
    claims = wikidata_game.get(u'claims')
    if property in claims[u'claims']:
        print(f'Skipping: Already has a TheLegacy ID!')
    else:
        legacy_claim = pywikibot.Claim(repo, property)
        legacy_claim.setTarget(str(id))
        wikidata_game.addClaim(legacy_claim, summary=f'based on {base} ID.')
        print(f"Adding TheLegacy ID.")


def harvest_c64wiki():
    category = pywikibot.Category(site, 'Kategorie:Spiel')
    for page in list(category.articles()):
        if page.title() == "C64-Wiki:Musterartikel (Spiel)":
            continue

        if page.title() == "Testartikel (Spielvoting)":
            continue

        if "Übungsartikel" in page.title():
            continue

        print(page.title())

        wikidata_game = find_item_from_external_id('P11129', str(page.title().replace(' ', '_')))
        if wikidata_game is not None:
            print(f"Found matching Wikidata {wikidata_game}")

            thelegacy_id = extract_template_id(page.text)
            if not thelegacy_id is None:
                claim_legacy(wikidata_game, u'P12709', thelegacy_id, 'C64-Wiki')
        else:
            print(f"No match in Wikidata.")

        print()


class ArchivedKultboyPage():

    def __init__(self, kultboy_id):
        url = f"https://web.archive.org/web/0/http://www.kultboy.com/testbericht-uebersicht/{str(kultboy_id)}/"
        print(f"Processing URL {url}")
        try:
            request = urllib.request.Request(url, None)
            response = urllib.request.urlopen(request)
            html = response.read().decode("ISO-8859-1")
        except Exception as error:
            pywikibot.output(error)
            return

        self.html = html

    def get_thelegacy_id(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        link = soup.find('a', title="The Legacy bietet umfassende Informationen über dieses Spiel an.")
        if link:
            href = link['href']
            match = re.search(r'/Museum/(\d+)/', href)
            if match:
                return match.group(1)
            else:
                None
        else:
            None


def harvest_kultboy():
    for kultboy_id in range(1, 1293):
        time.sleep(5)
        kultboy_page = ArchivedKultboyPage(kultboy_id)
        thelegacy_id = kultboy_page.get_thelegacy_id()
        if thelegacy_id is None:
            continue

        wikidata_game = find_item_from_external_id('P10850', kultboy_id)
        if wikidata_game is not None:
            print(f"Found matching Wikidata {wikidata_game}")
            claim_legacy(wikidata_game, u'P12709', thelegacy_id, 'Kultboy')
        else:
            print(f"No match in Wikidata.")

        print()


class ArchivedTheLegacyPage():

    def __init__(self, kultboy_id):
        url = f"https://web.archive.org/web/2017/http://www.thelegacy.de/Museum/{str(kultboy_id)}/"
        print(f"Processing URL {url}")
        try:
            request = urllib.request.Request(url, None)
            response = urllib.request.urlopen(request)
            html = response.read().decode("ISO-8859-1")
        except Exception as error:
            pywikibot.output(error)
            return

        self.html = html

    def get_kultboy_id(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        img = soup.find('img', src=lambda src: src and 'kultboy_link.gif' in src)
        if img is None:
            return None

        a = img.find_parent('a')
        if a and 'href' in a.attrs:
            href = a['href']
            match = re.search(r'id=(\d+)', href)
            if match:
                return match.group(1)
            else:
                return None


def match_kultboy():
    for thelegacy_id in range(10, 11811):
        time.sleep(5)
        kultboy_page = ArchivedTheLegacyPage(thelegacy_id)
        kultboy_id = kultboy_page.get_kultboy_id()

        if kultboy_id is None:
            print()
            continue

        wikidata_game = find_item_from_external_id('P10850', kultboy_id)
        if wikidata_game is not None:
            print(f"Found matching Wikidata {wikidata_game}")
            claim_legacy(wikidata_game, u'P12709', thelegacy_id, 'Kultboy')
        else:
            print(f"No match in Wikidata.")

        print()

#harvest_c64wiki()
#harvest_kultboy()
match_kultboy()