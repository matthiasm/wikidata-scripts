#!/usr/bin/python3

import pywikibot
import re
from pywikibot.data.sparql import SparqlQuery
import requests

site = pywikibot.Site(url='https://www.c64-wiki.de', fam='c64')

repo = pywikibot.Site()
repo.login()


def fetch_games_pages():
    category = pywikibot.Category(site, 'Kategorie:Spiel')
    pages = list(category.articles())
    return pages


def extract_template_id(source_content, database):
    match = re.search(rf"{{{database}\|(\d+)}}", source_content)
    if match:
        return match.group(1)
    else:
        return None

def extract_template_slug(page, edition):
    if f"{{{edition}}}" in page.text:
        return page.title()

    match = re.search(rf"{{{edition}\|([\w\s\(\)]+)}}", page.text)
    if match:
        return match.group(1)
    else:
        return None

def find_item_from_external_id(property, id):
    sparql = SparqlQuery()
    try:
        results = sparql.select(f"""
            SELECT ?item WHERE {{
            ?item wdt:{property} "{id}" .
            }}
        """)
    except Exception as error:
        pywikibot.error("Failed to query for {id}", error)
        return None

    if len(results) == 0:
        return None
    match = re.match(r"^https?://www\.wikidata\.org/entity/(Q\d+)$", results[0]["item"])
    if not match:
        return None
    return pywikibot.ItemPage(repo, match.group(1))


def database_claim(page, property, template_id, identifier_name):
    wikidata_game = find_item_from_external_id(property, extract_template_id(page.text, template_id))
    if wikidata_game is not None:
        print(f"Found matching Wikidata {wikidata_game}")

        claims = wikidata_game.get(u'claims')
        if u'P11129' in claims[u'claims']:
            print(u'Skipping: Already has a C64-Wiki.de game ID!')
        else:
            c64wiki_claim = pywikibot.Claim(repo, u'P11129')
            c64wiki_claim.setTarget(str(page.title().replace(' ', '_')))
            wikidata_game.addClaim(c64wiki_claim, summary=u'based on ' + identifier_name)
            print(f"Adding C64-Wiki.de game ID.")

        print()
        return wikidata_game

    return None


def get_wikidata_item_from_wikipedia_slug(title, lang):
    url = f"https://{lang}.wikipedia.org/w/api.php"
    params = {
        'action': 'query',
        'prop': 'pageprops',
        'titles': title,
        'format': 'json'
    }

    response = requests.get(url, params=params)
    data = response.json()
    pages = data.get('query', {}).get('pages', {})
    if pages:
        for page_id in pages:
            page = pages[page_id]
            wikibase_item = page.get('pageprops', {}).get('wikibase_item')
            if wikibase_item is not None:
                return pywikibot.ItemPage(repo, wikibase_item)

    return None


def german_wikipedia_claim(page):
    title = extract_template_slug(page, "Wikipedia")
    if title is None:
        return

    wikidata_game = get_wikidata_item_from_wikipedia_slug(title, 'de')
    if wikidata_game is not None:
        print(f"Found matching Wikidata {wikidata_game}")

        claims = wikidata_game.get(u'claims')
        if u'P11129' in claims[u'claims']:
            print(u'Skipping: Already has a C64-Wiki.de game ID!')
        else:
            c64wiki_claim = pywikibot.Claim(repo, u'P11129')
            c64wiki_claim.setTarget(str(page.title().replace(' ', '_')))
            wikidata_game.addClaim(c64wiki_claim, summary=u'based on German Wikipedia title')
            print(f"Adding C64-Wiki ID.")

        print()
        return wikidata_game

    return None


def english_wikipedia_claim(page):
    title = extract_template_slug(page, "WikipediaEN")
    if title is None:
        return

    wikidata_game = get_wikidata_item_from_wikipedia_slug(title, 'en')
    if wikidata_game is not None:
        print(f"Found matching Wikidata {wikidata_game}")

        claims = wikidata_game.get(u'claims')
        if u'P11129' in claims[u'claims']:
            print(u'Skipping: Already has a C64-Wiki.de game ID!')
        else:
            c64wiki_claim = pywikibot.Claim(repo, u'P11129')
            c64wiki_claim.setTarget(str(page.title().replace(' ', '_')))
            wikidata_game.addClaim(c64wiki_claim, summary=u'based on English Wikipedia title')
            print(f"Adding C64-Wiki ID.")

        print()
        return wikidata_game

    return None


def match_game_databases():
    for page in fetch_games_pages():
        if page.title() == "C64-Wiki:Musterartikel (Spiel)":
            continue

        if page.title() == "Testartikel (Spielvoting)":
            continue

        print(page.title())

        wikidata_game = database_claim(page, 'P10850', 'kultboy' ,'Kultboy game ID')
        if wikidata_game is not None:
            continue

        wikidata_game = database_claim(page, 'P4816', 'lemon' ,'Lemon 64 ID')
        if wikidata_game is not None:
            continue

        wikidata_game = database_claim(page, 'P7852', 'c64com' ,'C64.COM ID')
        if wikidata_game is not None:
            continue

        wikidata_game = database_claim(page, 'P4916', 'ready64' ,'Ready64 ID')
        if wikidata_game is not None:
            continue

        # multiple IDs per game:

        wikidata_game = database_claim(page, 'P7853', 'c64games' ,'c64games.de ID')
        if wikidata_game is not None:
            continue

        wikidata_game = database_claim(page, 'P4917', 'gamebase' ,'Gamebase64 ID')
        if wikidata_game is not None:
            continue

        print()


def match_wikipedia():
    for page in site.allpages():
        if page.title() == "C64-Wiki:Musterartikel (Spiel)":
            continue

        if page.title() == "Testartikel (Spielvoting)":
            continue

        print(page.title())

        wikidata_entity = german_wikipedia_claim(page)
        if wikidata_entity is not None:
            continue

        wikidata_entity = english_wikipedia_claim(page)
        if wikidata_entity is not None:
            continue

        print()

match_game_databases()
match_wikipedia()
