#!/usr/bin/python3

import pywikibot
import re
from pywikibot.data.sparql import SparqlQuery

site = pywikibot.Site(url='https://strategywiki.org', fam='strategy')

repo = pywikibot.Site()
repo.login()


def extract_template_parameter(page, id):
    match = re.search(rf"\|{id}\=([\w\s\(\)]+)\n", page.text)
    if match:
        return match.group(1).replace(" ", "_")
    else:
        return None


def find_item_from_external_id(property, id):
    sparql = SparqlQuery()
    try:
        results = sparql.select(f"""
            SELECT ?item WHERE {{
            ?item wdt:{property} "{id}" .
            }}
        """)
    except Exception as error:
        pywikibot.error("Failed to query for {id}", error)
        return None

    if len(results) == 0:
        return None
    match = re.match(r"^https?://www\.wikidata\.org/entity/(Q\d+)$", results[0]["item"])
    if not match:
        return None
    return pywikibot.ItemPage(repo, match.group(1))


def database_claim(page, property, template_id, identifier_name):
    wikidata_game = find_item_from_external_id(property, extract_template_parameter(page, template_id))
    if wikidata_game is not None:
        print(f"Found matching Wikidata {wikidata_game}")

        claims = wikidata_game.get(u'claims')
        if u'P9075' in claims[u'claims']:
            print(u'Skipping: Already has a StrategyWiki ID!')
        else:
            c64wiki_claim = pywikibot.Claim(repo, u'P9075')
            title = str(page.title().replace(' ', '_')).replace('%26', '&').replace('%27', '\'')
            c64wiki_claim.setTarget(title)
            wikidata_game.addClaim(c64wiki_claim, summary=u'based on ' + identifier_name)
            print(f"Adding StrategyWiki ID.")

        print()
        return wikidata_game

    return None


category = pywikibot.Category(site, 'Category:Games')
for page in list(category.articles()):
    print(page.title())

    wikidata_game = database_claim(page, 'P6337', 'pcgamingwiki' ,'PCGamingWiki ID')
    if wikidata_game is not None:
        continue

    wikidata_game = database_claim(page, 'P5797', 'twitch' ,'Twitch channel ID')
    if wikidata_game is not None:
        continue

    wikidata_game = database_claim(page, 'P2002', 'twitter' ,'Twitter username')
    if wikidata_game is not None:
        continue

    wikidata_game = database_claim(page, 'P2013', 'facebook' ,'Facebook username')
    if wikidata_game is not None:
        continue

    wikidata_game = database_claim(page, 'P856', 'website' ,'official website')
    if wikidata_game is not None:
        continue

    print()
