#!/usr/bin/python3

import re
import pywikibot
import urllib.request
from bs4 import BeautifulSoup
from pywikibot.data.sparql import SparqlQuery

site = pywikibot.Site(url='https://www.pcgamingwiki.com', fam='pcgaming')

repo = pywikibot.Site()
repo.login()


def harvest_pcgamingwiki(start=None):
    found_start = start is None
    category = pywikibot.Category(site, 'Category:Games')
    for page in list(category.articles()):

        if not found_start:
            if page.title() == start:
                found_start = True
            continue

        print(page.title())

        hltb_id = extract_template_parameter(page, 'hltb')
        if hltb_id is None:
            print()
            continue

        wikidata_game = find_item_from_external_id('P2816', hltb_id)
        if wikidata_game is not None:
            print(f"Found matching Wikidata {wikidata_game}")

            claims = wikidata_game.get(u'claims')
            if u'P2816' in claims[u'claims']:
                print(u'Skipping: Already has a HowLongToBeat ID!')
            else:
                htlb_claim = pywikibot.Claim(repo, u'P2816')
                htlb_claim.setTarget(str(hltb_id))
                wikidata_game.addClaim(htlb_claim, summary=u'based on PCGamingWiki ID')
                print(f"Adding HowLongToBeat ID.")

        print()


def extract_template_parameter(page, id):
    try:
        match = re.search(rf"\|{id}\s+\=\s+([\w\s\-\(\)]+)\n", page.text)
        if match:
            return match.group(1).replace(" ", "_")
        else:
            return None
    except Exception as error:
            pywikibot.output(error)
            return None


def find_item_from_external_id(property, id):
    sparql = SparqlQuery()
    try:
        results = sparql.select(f"""
            SELECT ?item WHERE {{
            ?item wdt:{property} "{id}" .
            }}
        """)
    except Exception as error:
        pywikibot.error("Failed to query for {id}", error)
        return None

    if len(results) == 0:
        return None
    match = re.match(r"^https?://www\.wikidata\.org/entity/(Q\d+)$", results[0]["item"])
    if not match:
        return None
    return pywikibot.ItemPage(repo, match.group(1))


class HowLongToBeatEntry():

    def __init__(self, hltb_id):
        url = f"https://howlongtobeat.com/game/{str(hltb_id)}"
        pywikibot.output(f"Processing URL {url}")
        try:
            request = urllib.request.Request(url, None)
            response = urllib.request.urlopen(request)
            self.html = response.read().decode("utf-8")
        except Exception as error:
            pywikibot.output(error)
            return

    def get_steam_id(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        a = soup.find('a', class_=lambda x: x and 'steam' in x.lower())
        if a is not None:
            match = re.match(r'^https:\/\/(?:store\.)?steampowered\.com\/app\/(\d+)', a['href'])
            if match:
                return match.group(1)

    def get_gog_id(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        a = soup.find('a', class_=lambda x: x and 'gog' in x.lower()) # TODO: does not work
        if a is not None:
            match = re.search(r'https:\/\/www\.gog\.com\/en\/game\/(\w+)', a['href'])
            if match:
                return match.group(1)


def match():
    for hltb_id in range(1602, 152290):
        hltb_page = HowLongToBeatEntry(hltb_id)

        steam_id = hltb_page.get_steam_id()
        if not steam_id is None:
            wikidata_game = find_item_from_external_id('P1733', steam_id)
            if wikidata_game is not None:
                print(f"Found matching Wikidata {wikidata_game}")

                claims = wikidata_game.get(u'claims')
                if u'P2816' in claims[u'claims']:
                    print(u'Skipping: Already has a HowLongToBeat ID!')
                else:
                    hltb_claim = pywikibot.Claim(repo, u'P2816')
                    hltb_claim.setTarget(str(hltb_id))
                    wikidata_game.addClaim(hltb_claim, summary=u'based on Steam ID.')
                    print(f"Adding HowLongToBeat ID.")

                print()
                continue
            else:
                print(f"No match in Wikidata.")

        print()

if __name__ == "__main__":
    #harvest_pcgamingwiki('A Top-Down Job: Blood Gain')
    match()
