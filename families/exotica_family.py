"""
This family file was auto-generated by generate_family_file.py script.

Configuration parameters:
  url = https://www.exotica.org.uk/wiki/
  name = exotica

Please do not commit this to the Git repository!
"""
from pywikibot import family


class Family(family.Family):  # noqa: D101

    name = 'exotica'
    langs = {
        'en-gb': 'www.exotica.org.uk',
    }

    def scriptpath(self, code):
        return {
            'en-gb': '/mediawiki',
        }[code]

    def protocol(self, code):
        return {
            'en-gb': 'https',
        }[code]
