#!/usr/bin/python3

import urllib.request
import re
import csv
import pywikibot
from pywikibot.data.sparql import SparqlQuery
from bs4 import BeautifulSoup

repo = pywikibot.Site()
repo.login()

openretro_uuid_regex = re.compile(r'^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$')

class HallOfLightEntry():

    def __init__(self, hol_id):
        url = f"http://hol.abime.net/{str(hol_id)}"
        print(f"Processing URL {url}")
        try:
            request = urllib.request.Request(url, None)
            response = urllib.request.urlopen(request)
            html = response.read().decode("utf-8")
        except Exception as error:
            pywikibot.output(error)
            return

        self.html = html

    def get_title(self):
        if not hasattr(self, 'html'):
            return None
        soup = BeautifulSoup(self.html, 'html.parser')
        div_content = soup.find('div', id='game_title_inner')
        header = div_content.find('h1')
        return header.text if header else None

    def get_year(self):
        if not hasattr(self, 'html'):
            return None
        soup = BeautifulSoup(self.html, 'html.parser')
        year_link = soup.find('div', {'id': 'content_general_info'}).find('a')
        return year_link.text if year_link else None

    def get_whdload_id(self):
        if not hasattr(self, 'html'):
            return None
        soup = BeautifulSoup(self.html, 'html.parser')
        whdload_div = soup.find('div', text='WHDLoad')
        if whdload_div:
            row_div = whdload_div.find_parent('div', class_='hol_grid_item row')
            if row_div:
                a = row_div.find('a', href=True)
                if a and 'href' in a.attrs:
                    match = re.match(r"^https?://www\.whdload\.de/games/(\w+)\.html$", a['href'])
                    if match:
                        return match.group(1)

    def get_exotica_id(self):
        if not hasattr(self, 'html'):
            return None
        soup = BeautifulSoup(self.html, 'html.parser')
        exotica_div = soup.find('div', text='Exotica')
        if exotica_div:
            row_div = exotica_div.find_parent('div', class_='hol_grid_item row')
            if row_div:
                a = row_div.find('a', href=True)
                if a and 'href' in a.attrs:
                    return a['href'].split('/')[-1].replace('%20', '_')

    def get_openretro_id(self):
        if not hasattr(self, 'html'):
            return None
        soup = BeautifulSoup(self.html, 'html.parser')
        exotica_div = soup.find('div', text='OpenRetro')
        if exotica_div:
            row_div = exotica_div.find_parent('div', class_='hol_grid_item row')
            if row_div:
                a = row_div.find('a', href=True)
                if a and 'href' in a.attrs:
                    openretro_id = a['href'].split('/')[-1]
                    if openretro_uuid_regex.match(openretro_id):
                        return openretro_id
                    else:
                        game = OpenRetroAmigaGame(openretro_id)
                        return game.get_uuid()

    def get_sps_id(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        sps_div = soup.find('div', text='SPS')
        if sps_div:
            row_div = sps_div.find_parent('div', class_='hol_grid_item row')
            if row_div:
                a = row_div.find('a', href=True)
                if a and 'href' in a.attrs:
                    match = re.match(r"^https?://www\.softpres\.org/\?id=:games&gameid=(\d+)$", a['href'])
                    if match:
                        return match.group(1)

    def get_lemon_id(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        lemon_div = soup.find('div', text='Lemon Amiga')
        if lemon_div:
            row_div = lemon_div.find_parent('div', class_='hol_grid_item row')
            if row_div:
                a = row_div.find('a', href=True)
                if a and 'href' in a.attrs:
                    return a['href'].split('game_id=')[-1]


class OpenRetroAmigaGame():

    def __init__(self, slug):
        url = f"https://openretro.org/amiga/{slug}"
        print(f"Processing URL {url}")
        try:
            request = urllib.request.Request(url, None)
            response = urllib.request.urlopen(request)
            html = response.read().decode("utf-8")
        except Exception as error:
            pywikibot.output(error)
            return

        self.html = html

    def get_uuid(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        return soup.find('input', {'name': 'game'})['value']


def find_item_from_external_id(property, id):
    sparql = SparqlQuery()
    try:
        results = sparql.select(f"""
            SELECT ?item WHERE {{
            ?item wdt:{property} "{id}" .
            }}
        """)
    except Exception as error:
        pywikibot.error("Failed to query for {id}", error)
        return None

    if len(results) == 0:
        return None
    match = re.match(r"^https?://www\.wikidata\.org/entity/(Q\d+)$", results[0]["item"])
    if not match:
        return None
    return pywikibot.ItemPage(repo, match.group(1))


def database_claim(hol_id, property, external_id, identifier_name):
    wikidata_game = find_item_from_external_id(property, external_id)
    if wikidata_game is not None:
        print(f"Found matching Wikidata {wikidata_game}")

        claims = wikidata_game.get(u'claims')
        if u'P4671' in claims[u'claims']:
            print(u'Skipping: Already has a Hall of Light ID!')
        else:
            claim = pywikibot.Claim(repo, u'P4671')
            claim.setTarget(str(hol_id))
            wikidata_game.addClaim(claim, summary=u'based on ' + identifier_name)
            print(f"Adding Hall of Light ID.")

        print()
        return wikidata_game

    return None


def match_hol():
    for hol_id in range(1, 6999):
        entry = HallOfLightEntry(hol_id)

        wikidata_game = database_claim(hol_id, 'P4846', entry.get_lemon_id(), 'Lemon Amiga ID.')
        if wikidata_game is not None:
            continue

        wikidata_game = database_claim(hol_id, 'P12225', entry.get_whdload_id(), 'WHDLoad ID.')
        if wikidata_game is not None:
            continue

        wikidata_game = database_claim(hol_id, 'P7683', entry.get_openretro_id(), 'OpenRetro Game Database ID.')
        if wikidata_game is not None:
            continue

        wikidata_game = database_claim(hol_id, 'P7981', entry.get_exotica_id(), 'ExoticA ID')
        if wikidata_game is not None:
            continue

        wikidata_game = database_claim(hol_id, 'P7516', entry.get_sps_id(), 'Software Preservation Society ID')
        if wikidata_game is not None:
            continue

        print()


def import_hol():

    with open("hol.tsv", mode='w', newline='') as file:
        writer = csv.writer(file, delimiter='\t', quotechar="'")

        for hol_id in range(1, 6999):

            wikidata_game = find_item_from_external_id(u'P4671', hol_id)
            if wikidata_game is not None:
                print(f"Found matching {wikidata_game} based on Hall of Light ID")
                print()
                continue

            entry = HallOfLightEntry(hol_id)
            if entry.get_year() is None:
                print("No release date")
                print()
                continue

            wikidata_game = find_item_from_external_id(u'P7981', entry.get_exotica_id())
            if wikidata_game is not None:
                print(f"Found matching {wikidata_game} based on Exotica ID")
                print()
                continue

            wikidata_game = find_item_from_external_id(u'P4846', entry.get_lemon_id())
            if wikidata_game is not None:
                print(f"Found matching {wikidata_game} based on Lemon ID")
                print()
                continue

            wikidata_game = find_item_from_external_id(u'P7683', entry.get_openretro_id())
            if wikidata_game is not None:
                print(f"Found matching {wikidata_game} based on OpenRetro ID")
                print()
                continue

            wikidata_game = find_item_from_external_id(u'P12225', entry.get_whdload_id())
            if wikidata_game is not None:
                print(f"Found matching {wikidata_game} based on WHDLoad ID")
                print()
                continue

            writer.writerow(["CREATE"])
            writer.writerow(["LAST", "Len", f'"{entry.get_title()}"'])
            writer.writerow(["LAST", "Den", f'"{entry.get_year()} video game"'])
            writer.writerow(["LAST", "P31", "Q7889"])

            writer.writerow(["LAST", "P4671", f'"{hol_id}"' ])

            if entry.get_exotica_id() is not None:
                writer.writerow(["LAST", "P7981", f'"{entry.get_exotica_id()}"' ])

            if entry.get_lemon_id() is not None:
                writer.writerow(["LAST", "P4846", f'"{entry.get_lemon_id()}"' ])

            if entry.get_openretro_id() is not None:
                writer.writerow(["LAST", "P7683", f'"{entry.get_openretro_id()}"' ])

            if entry.get_sps_id() is not None:
                writer.writerow(["LAST", "P7516", f'"{entry.get_sps_id()}"' ])

            if entry.get_whdload_id() is not None:
                writer.writerow(["LAST", "P12225", f'"{entry.get_whdload_id()}"' ])

            file.flush()
            print()


#match_hol()
import_hol()
