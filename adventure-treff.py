#!/usr/bin/python3

import urllib.request
import re
import pywikibot
from pywikibot.data.sparql import SparqlQuery
from bs4 import BeautifulSoup

repo = pywikibot.Site()
repo.login()

class GamePage():

    def __init__(self, game_id):
        url = f"https://www.adventure-treff.de/spiele-datenbank/{str(game_id)}"
        print(f"Processing URL {url}")
        try:
            request = urllib.request.Request(url, None)
            response = urllib.request.urlopen(request)
            html = response.read().decode("utf-8")
        except Exception as error:
            pywikibot.output(error)
            return

        self.html = html

def get_title(self):
    if not hasattr(self, 'html'):
        return None

    soup = BeautifulSoup(self.html, 'lxml')
    h2 = soup.find('h2')
    if h2:
        return h2.text.strip()

    return None

def get_gog(self):
    if not hasattr(self, 'html'):
        return None

    soup = BeautifulSoup(self.html, 'lxml')
    button = soup.find('img', {'src': lambda x: x and 'gog-botton.png' in x})
    link = button.find_parent('a') if button else None
    url = link['href'].split('gog.com/')[-1] if link else None
    if url is None:
        return None
    if '?' in url:
        return url.split('?')[0]
    return url

def get_apple(self):
    if not hasattr(self, 'html'):
        return None

    soup = BeautifulSoup(self.html, 'lxml')
    button = soup.find('img', {'src': lambda x: x and 'app-store-botton.png' in x})
    link = button.find_parent('a') if button else None
    match = re.search(r"id(\d+)", link['href']) if link else None
    return match.group(1) if match else None

def get_google_play(self):
    if not hasattr(self, 'html'):
        return None

    soup = BeautifulSoup(self.html, 'lxml')
    button = soup.find('img', {'src': lambda x: x and 'button_googleplay.png' in x})
    link = button.find_parent('a') if button else None
    url = link['href'].split('https://play.google.com/store/apps/details?id=')[-1] if link else None
    if url is None:
        return None
    if '?' in url:
        return url.split('?')[0]
    if '&' in url:
        return url.split('&')[0]
    return url

def find_item(handle, property):
    print("Query handle " + handle)
    sparql = SparqlQuery()
    try:
        result = sparql.select(f"""
            SELECT ?item WHERE {{
                ?item wdt:{property} ?handleValue .
                FILTER (lcase(?handleValue) = lcase("{handle}"))
            }}
        """)
    except Exception as error:
        pywikibot.output(error)
        return None

    if len(result) != 1:
        return None

    match = re.match(r"^https?://www\.wikidata\.org/entity/(Q\d+)$", result[0]["item"])
    if not match:
        return None

    return pywikibot.ItemPage(repo, match.group(1))

def get_game_ids(page):
    url = f"https://www.adventure-treff.de/spiele/alle-spiele?page={str(page)}"
    pywikibot.output(f"Processing URL {url}")
    pywikibot.output()
    try:
        request = urllib.request.Request(url, None)
        response = urllib.request.urlopen(request)
        html = response.read().decode("utf-8")
    except Exception as error:
        pywikibot.output(error)
        return

    soup = BeautifulSoup(html, 'lxml')
    div_tags = soup.find_all('div', class_="col-md-15")
    for div in div_tags:
        for title in div.find_all('h2'):
            link = title.find('a', href=re.compile("/spiele-datenbank/"))
            if link:
                href = link['href']
                match = re.search(r'/spiele-datenbank/(\d+)-', href)
                if match:
                    yield match.group(1)

if __name__ == "__main__":
    for page in range(39,117):
        for game_id in get_game_ids(page):
            if game_id is not None:
                game_page = GamePage(game_id)
                title = get_title(game_page)
                if not title:
                    print("Skipping empty page.")
                    continue
                print(f"Game: {title}")

                wikidata_entity = None
                gog_id = get_gog(game_page)
                if gog_id is not None:
                    wikidata_entity = find_item(gog_id, "P2725")

                if wikidata_entity is None:
                    apple_id = get_apple(game_page)
                    if apple_id is not None:
                        wikidata_entity = find_item(apple_id, "P3861")

                if wikidata_entity is None:
                    play_store_id = get_google_play(game_page)
                    if play_store_id is not None:
                        wikidata_entity = find_item(play_store_id, "P3418")

                if wikidata_entity is not None:
                    print(f"Found matching Wikidata {wikidata_entity}")

                    claims = wikidata_entity.get(u'claims')
                    if u'P12291' in claims[u'claims']:
                        print(u'Already has an Adventure-Treff game ID!')
                    else:
                        claim = pywikibot.Claim(repo, u'P12291')
                        claim.setTarget(str(game_id))
                        wikidata_entity.addClaim(claim, summary=u'Add Adventure-Treff game ID based on store ID.')
                        print(f"Adding Adventure-Treff game ID")
                else:
                    print(f"Error: no match in Wikidata.")

                print()
