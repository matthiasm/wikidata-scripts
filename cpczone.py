#!/usr/bin/python3

import urllib.request
import re
import pywikibot
from pywikibot.data.sparql import SparqlQuery
from bs4 import BeautifulSoup

repo = pywikibot.Site()
repo.login()

class DatabaseEntry():

    def __init__(self, cpczone_id):
        url = f"https://www.cpcwiki.eu/zone/game/{str(cpczone_id)}/default.htm"
        print(f"Processing URL {url}")
        try:
            request = urllib.request.Request(url, None)
            response = urllib.request.urlopen(request)
            html = response.read().decode("utf-8")
        except Exception as error:
            pywikibot.output(error)
            return

        self.html = html

    def get_lemon64_id(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        a = soup.find('a', string='Lemon 64')
        if a and 'href' in a.attrs:
            if '@game_id=' in a['href']:
                return a['href'].split('@game_id=')[1]

    def get_lemon_amiga_id(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        a = soup.find('a', string='Lemon Amiga')
        if a and 'href' in a.attrs:
            if '@game_id=' in a['href']:
                return a['href'].split('@game_id=')[1]

    def get_world_of_spectrum_id(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        a = soup.find('a', string='World of Spectrum')
        if a and 'href' in a.attrs:
            if 'infoseekid.cgi@id=' in a['href']:
                return a['href'].split('infoseekid.cgi@id=')[1]

    def get_generation_msx_id(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        a = soup.find('a', string='Generation MSX')
        if a and 'href' in a.attrs:
            match = re.search(r'www\.generation-msx\.nl/msxdb/softwareinfo/(\d+)', a['href'])
            if match:
                return match.group(1)


def find_item(handle, property):
    print("Query handle " + handle)
    sparql = SparqlQuery()
    try:
        result = sparql.select(f"""
            SELECT ?item WHERE {{
                ?item wdt:{property} ?handleValue .
                FILTER (lcase(?handleValue) = lcase("{handle}"))
            }}
        """)
    except Exception as error:
        pywikibot.output(error)
        return None

    if len(result) != 1:
        return None

    match = re.match(r"^https?://www\.wikidata\.org/entity/(Q\d+)$", result[0]["item"])
    if not match:
        return None

    return pywikibot.ItemPage(repo, match.group(1))


def add(game_id, wikidata_entity):
    print(f"Found matching Wikidata {wikidata_entity}")

    claims = wikidata_entity.get(u'claims')
    if u'P13299' in claims[u'claims']:
        print(u'Already has a CPC Zone game ID!')
    else:
        claim = pywikibot.Claim(repo, u'P13299')
        claim.setTarget(str(game_id))
        wikidata_entity.addClaim(claim, summary=u'Add CPC Zone game ID based on other resource.')
        print(f"Adding CPC Zone game ID")


for game_id in range(1,3377):
    entry = DatabaseEntry(game_id)

    wikidata_entity = None

    lemon64_id = entry.get_lemon64_id()
    if lemon64_id is not None:
        wikidata_entity = find_item(lemon64_id, "P4816")
        if wikidata_entity is not None:
            add(game_id, wikidata_entity)

    lemon_amiga_id = entry.get_lemon_amiga_id()
    if lemon_amiga_id is not None:
        wikidata_entity = find_item(lemon_amiga_id, "P4846")
        if wikidata_entity is not None:
            add(game_id, wikidata_entity)

    zxdb_id = entry.get_world_of_spectrum_id()
    if zxdb_id is not None:
        wikidata_entity = find_item(zxdb_id, "P4705")
        if wikidata_entity is not None:
            add(game_id, wikidata_entity)

    generation_msx_id = entry.get_generation_msx_id()
    if generation_msx_id is not None:
        wikidata_entity = find_item(generation_msx_id, "P4960")
        if wikidata_entity is not None:
            add(game_id, wikidata_entity)

    if wikidata_entity is None:
        print(f"Error: no match in Wikidata.")

    print()
