#!/usr/bin/python3

import re
import pywikibot
import time
from pywikibot.data.sparql import SparqlQuery
from webdriver_manager.chrome import ChromeDriverManager
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException

repo = pywikibot.Site()
repo.login()

options = webdriver.ChromeOptions()
options.headless = True
options.page_load_strategy = 'none'
chrome_path = ChromeDriverManager().install()
chrome_service = Service(chrome_path)
driver = webdriver.Chrome(options=options, service=chrome_service)
driver.implicitly_wait(5)

class PesticideInfoPage():

    def __init__(self, pri):
        match = re.match(r"https://www\.pesticideinfo\.org/chemical/PRI(\d+)/?", str(pri))
        if match:
            pri = match.group(1)
        else:
            pri = str(pri).strip()

        url = f"https://www.pesticideinfo.org/chemical/PRI{str(pri)}"
        print(f"Processing URL {url}")
        try:
            driver.get(url)
            time.sleep(5)
            data = driver.find_element(By.CSS_SELECTOR, "div[class*='data-table'")
            cas_number_element = data.find_element(By.XPATH, ".//div[div[contains(text(), 'CAS number')]]/div[2]")
            cas = cas_number_element.text
        except NoSuchElementException:
            self.cas = None
            print(f"Failed to find CAS.")
            return
        except Exception as error:
            pywikibot.stdout(error)
            return

        print(f"Extracted CAS {cas}")

        self.pri = pri
        self.cas = cas.strip()

def find_item_for_cas(cas):
    sparql = SparqlQuery()
    result = sparql.select(f"""
        SELECT ?item WHERE {{
          ?item wdt:P231 "{cas}" .
        }}
    """)
    if len(result) != 1:
        return None
    match = re.match(r"^https?://www\.wikidata\.org/entity/(Q\d+)$", result[0]["item"])
    if not match:
        return None
    return pywikibot.ItemPage(repo, match.group(1))

if __name__ == "__main__":
    for cameo_id in range(3250, 6000):
        cameo_page = PesticideInfoPage(cameo_id)
        if not cameo_page.cas is None:
            wikidata_chemical = find_item_for_cas(cameo_page.cas)
            if wikidata_chemical is not None:
                print(f"Found matching Wikidata {wikidata_chemical}")

                claims = wikidata_chemical.get(u'claims')
                if u'P11949' in claims[u'claims']:
                    print(u'Already has a PesticideInfo chemical ID!')
                else:
                    cas_claim = pywikibot.Claim(repo, u'P11949')
                    cas_claim.setTarget('PRI' + str(cameo_id))
                    wikidata_chemical.addClaim(cas_claim, summary=u'Add PesticideInfo chemical ID based on CAS registry number.')
                    print(f"Adding PesticideInfo chemical ID")
            else:
                print(f"No match in Wikidata.")
