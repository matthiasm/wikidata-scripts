#!/usr/bin/python3

import pywikibot
import re

site = pywikibot.Site('en', 'wikipedia')

repo = pywikibot.Site()
repo.login()

def extract_id_from_template(page, template):
    match = re.search(r"\{\{" + re.escape(template) + r"\|\s*([^\|\}]+)", page.text, re.IGNORECASE)
    if match:
        return match.group(1)
    else:
        return None


def claim(page, template):
    if not page.exists():
        return

    foldoc_id = extract_id_from_template(page, template.title(with_ns=False))
    if foldoc_id is None:
        return

    wikidata_item = page.data_item()
    if wikidata_item is not None:
        print(f"Found matching Wikidata {wikidata_item.getID()}")

        claims = wikidata_item.get(u'claims')
        if u'P12946' in claims[u'claims']:
            print(u'Skipping: Already has a FOLDOC ID!')
        else:
            foldoc_claim = pywikibot.Claim(repo, u'P12946')
            foldoc_claim.setTarget(str(foldoc_id))
            wikidata_item.addClaim(foldoc_claim, summary=u'based on English Wikipedia template')
            print(f"Adding FOLDOC ID.")

        print()


def harvest_templates():
    template_page = pywikibot.Page(site, f'Template:FOLDOC')
    templates_including_redirects = list(template_page.redirects())
    templates_including_redirects.append(template_page)
    for template in templates_including_redirects:
        for page in template.getReferences(namespaces=0, follow_redirects=False):
            claim(page, template)


harvest_templates()

