#!/usr/bin/python3

import urllib.request
import re
import pywikibot
from pywikibot.data.sparql import SparqlQuery
from bs4 import BeautifulSoup

repo = pywikibot.Site()
repo.login()

class DatabaseEntry():

    def __init__(self, kultboy_id):
        url = f"https://www.kultboy.com/testbericht-uebersicht/{str(kultboy_id)}/"
        print(f"Processing URL {url}")
        try:
            request = urllib.request.Request(url, None)
            response = urllib.request.urlopen(request)
            html = response.read().decode("ISO-8859-1")
        except Exception as error:
            pywikibot.output(error)
            return

        self.html = html

    def get_c64_id(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        a = soup.find('a', href=lambda href: href and 'www.c64-wiki.de' in href)
        if a and 'href' in a.attrs:
            return a['href'].split('/')[-1]
        else:
            return None

    def get_hol_id(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        a_tag_with_title = soup.find('a', title="Umfangreiche Amiga Spiele-Datenbank")
        if a_tag_with_title and 'href' in a_tag_with_title.attrs:
            return a_tag_with_title['href'].split('/')[-1]
        else:
            return None

    def get_gog_id(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        img_tag_with_title = soup.find('img', title="GOG.com")
        if img_tag_with_title:
            a_tag = img_tag_with_title.find_parent('a')
            if a_tag and 'href' in a_tag.attrs:
                parsed_url = urllib.parse.urlparse(a_tag['href'])
                return parsed_url.path.lstrip('/')
            else:
                return None


def find_item_from_external_id(property, id):
    sparql = SparqlQuery()
    try:
        results = sparql.select(f"""
            SELECT ?item WHERE {{
            ?item wdt:{property} "{id}" .
            }}
        """)
    except Exception as error:
        pywikibot.error("Failed to query for {id}", error)
        return None

    if len(results) == 0:
        return None
    match = re.match(r"^https?://www\.wikidata\.org/entity/(Q\d+)$", results[0]["item"])
    if not match:
        return None
    return pywikibot.ItemPage(repo, match.group(1))


if __name__ == "__main__":
    for kultboy_id in range(1, 11397):
        kultboy_page = DatabaseEntry(kultboy_id)

        c64_id = kultboy_page.get_c64_id()
        if not c64_id is None:
            wikidata_game = find_item_from_external_id('P11129', c64_id)
            if wikidata_game is not None:
                print(f"Found matching Wikidata {wikidata_game}")

                claims = wikidata_game.get(u'claims')
                if u'P10850' in claims[u'claims']:
                    print(u'Skipping: Already has a Kultboy video game ID!')
                    print()
                    continue
                else:
                    kultboy_claim = pywikibot.Claim(repo, u'P10850')
                    kultboy_claim.setTarget(str(kultboy_id))
                    wikidata_game.addClaim(kultboy_claim, summary=u'based on C64-Wiki ID.')
                    print(f"Adding Kultboy video game ID.")
            else:
                print(f"No match in Wikidata.")

        hol_id = kultboy_page.get_hol_id()
        if not hol_id is None:
            wikidata_game = find_item_from_external_id('P4671', hol_id)
            if wikidata_game is not None:
                print(f"Found matching Wikidata {wikidata_game}")

                claims = wikidata_game.get(u'claims')
                if u'P10850' in claims[u'claims']:
                    print(u'Skipping: Already has a Kultboy video game ID!')
                    print()
                    continue
                else:
                    kultboy_claim = pywikibot.Claim(repo, u'P10850')
                    kultboy_claim.setTarget(str(kultboy_id))
                    wikidata_game.addClaim(kultboy_claim, summary=u'based on Hall of Light ID.')
                    print(f"Adding Kultboy video game ID.")
            else:
                print(f"No match in Wikidata.")

        gog_id = kultboy_page.get_gog_id()
        if not gog_id is None:
            wikidata_game = find_item_from_external_id('P2725', gog_id)
            if wikidata_game is not None:
                print(f"Found matching Wikidata {wikidata_game}")

                claims = wikidata_game.get(u'claims')
                if u'P10850' in claims[u'claims']:
                    print(u'Skipping: Already has a Kultboy video game ID!')
                    print()
                    continue
                else:
                    kultboy_claim = pywikibot.Claim(repo, u'P10850')
                    kultboy_claim.setTarget(str(kultboy_id))
                    wikidata_game.addClaim(kultboy_claim, summary=u'based on GOG.com ID.')
                    print(f"Adding Kultboy video game ID.")
            else:
                print(f"No match in Wikidata.")

        print()
