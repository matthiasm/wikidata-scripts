#!/usr/bin/python3

import urllib.request
import re
import pywikibot
from pywikibot.data.sparql import SparqlQuery
from bs4 import BeautifulSoup

site = pywikibot.Site(url='https://www.c64-wiki.de', fam='c64')

repo = pywikibot.Site()
repo.login()


class AbandoniaEntry():

    def __init__(self, abandonia_id):
        url = f"http://www.abandonia.com/games/{str(abandonia_id)}"
        print(f"Processing URL {url}")
        try:
            request = urllib.request.Request(url, None)
            response = urllib.request.urlopen(request)
            html = response.read().decode("utf-8")
        except Exception as error:
            pywikibot.output(error)
            return

        self.html = html


    def get_id(self, image):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        button = soup.find('img', {'src': lambda x: x and image in x})
        if button is None:
            return None

        a = button.find_parent('a')
        url = a['href']
        match = re.search(r'game_id=(\d+)', url)
        if match:
            return match.group(1)
        else:
            return None


def find_item_from_external_id(property, id):
    sparql = SparqlQuery()
    try:
        results = sparql.select(f"""
            SELECT ?item WHERE {{
            ?item wdt:{property} "{str(id)}" .
            }}
        """)
    except Exception as error:
        pywikibot.error("Failed to query for {id}", error)
        return None

    if results is None:
        return None

    if len(results) == 0:
        return None

    match = re.match(r"^https?://www\.wikidata\.org/entity/(Q\d+)$", results[0]["item"])
    if not match:
        return None

    return pywikibot.ItemPage(repo, match.group(1))


def claim_lemon(wikidata_game, property, id, name, base):
    claims = wikidata_game.get(u'claims')
    if property in claims[u'claims']:
        print(f'Skipping: Already has a {name} ID!')
    else:
        lemon_claim = pywikibot.Claim(repo, property)
        lemon_claim.setTarget(str(id))
        wikidata_game.addClaim(lemon_claim, summary=f'based on {base} ID.')
        print(f"Adding {name} ID.")


def harvest_abandonia():
    for abandonia_id in range(1, 1067):
        abandonia_page = AbandoniaEntry(abandonia_id)

        wikidata_game = find_item_from_external_id('P4962', abandonia_id)
        if wikidata_game is not None:
            print(f"Found matching Wikidata {wikidata_game}")
        else:
            print(f"No match in Wikidata.")
            print()
            continue

        lemon64_id = abandonia_page.get_id('c64.gif')
        if not lemon64_id is None:
            claim_lemon(wikidata_game, u'P4816', lemon64_id, 'Lemon64', 'Abandonia')

        lemonAmiga_id = abandonia_page.get_id('amiga.gif')
        if not lemonAmiga_id is None:
            claim_lemon(wikidata_game, u'P4846', lemonAmiga_id, 'Lemon Amiga', 'Abandonia')

        print()


def extract_template_id(source_content):
    match = re.search(r"{{lemon.*\|(\d+)}}", source_content)
    if match:
        return match.group(1)
    else:
        return None


def harvest_c64wiki():
    category = pywikibot.Category(site, 'Kategorie:Spiel')
    for page in list(category.articles()):
        if page.title() == "C64-Wiki:Musterartikel (Spiel)":
            continue

        if page.title() == "Testartikel (Spielvoting)":
            continue

        if "Übungsartikel" in page.title():
            continue

        print(page.title())

        wikidata_game = find_item_from_external_id('P11129', str(page.title().replace(' ', '_')))
        if wikidata_game is not None:
            print(f"Found matching Wikidata {wikidata_game}")

            lemon64_id = extract_template_id(page.text)
            if not lemon64_id is None:
                claim_lemon(wikidata_game, u'P4816', lemon64_id, 'Lemon64', 'C64-Wiki')
        else:
            print(f"No match in Wikidata.")

        print()


headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3'
}

class LemonAmigaEntry():

    def __init__(self, lemon_id):
        url = f"https://www.lemonamiga.com/games/details.php?id={str(lemon_id)}"
        print(f"Processing URL {url}")
        try:
            request = urllib.request.Request(url, headers=headers)
            response = urllib.request.urlopen(request)
            html = response.read().decode("utf-8")
        except Exception as error:
            pywikibot.output(error)
            return

        self.html = html

    def get_whdload_id(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        a = soup.find('a', title ='External link: Download installer at the WHDLoad Support Page')
        if a and 'href' in a.attrs:
            match = re.match(r"^https?://www\.whdload\.de/games/(\w+)\.html$", a['href'])
            if match:
                return match.group(1)
        else:
            return None

    def get_hol_id(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        a = soup.find('a', title="External link: Compare with HOL Amiga database")
        if a and 'href' in a.attrs:
            return a['href'].split('/')[-1]
        else:
            return None

    def get_mobygames_id(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        a = soup.find('a', title ='External link: More information on MobyGames')
        if a and 'href' in a.attrs:
            match = re.match(r"^https?://www\.mobygames\.com/game/(\d+).*$", a['href'])
            if match:
                return match.group(1)
        else:
            return None

    def get_sps_id(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        a = soup.find('a', title ='External link: Software Preservation Society')
        if a and 'href' in a.attrs:
            match = re.match(r"^https?://www\.softpres\.org/\?id=:games&gameid=(\d+)$", a['href'])
            if match:
                return match.group(1)
        else:
            return None

    def get_wikipedia_slug(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        a = soup.find('a', title ='External link: More information on Wikipedia')
        if a and 'href' in a.attrs:
            return a['href'].split('/')[-1]
        else:
            return None

    def get_uvl_id(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        a = soup.find('a', title ='External link: Compare with UVL database')
        if a and 'href' in a.attrs:
            match = re.match(r"^https?://www\.uvlist\.net/game-(\d+).*$", a['href'])
            if match:
                return match.group(1)
        else:
            return None

    def get_atarilegend_id(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        a = soup.find('a', title ='External link: Compare with Atari Legend database')
        if a and 'href' in a.attrs:
            match = re.match(r"^https?://www\.atarilegend\.com/games/(\d+)$", a['href'])
            if match:
                return match.group(1)
        else:
            return None

    def get_atarilegend_id(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        a = soup.find('a', title ='External link: Compare with Atari Legend database')
        if a and 'href' in a.attrs:
            match = re.match(r"^https?://www\.atarilegend\.com/games/(\d+)$", a['href'])
            if match:
                return match.group(1)
        else:
            return None


def database_claim(lemon_id, external_property, external_id, identifier_name, target_property, target_name):
    wikidata_game = find_item_from_external_id(external_property, external_id)
    if wikidata_game is not None:
        print(f"Found matching Wikidata {wikidata_game}")

        claims = wikidata_game.get(u'claims')
        if target_property in claims[u'claims']:
            print(f'Skipping: Already has a {target_name}!')
        else:
            lemonamiga_claim = pywikibot.Claim(repo, target_property)
            lemonamiga_claim.setTarget(str(lemon_id))
            wikidata_game.addClaim(lemonamiga_claim, summary=u'based on ' + identifier_name)
            print(f"Adding {target_name}")

        print()
        return wikidata_game

    return None


def match_lemonamiga():

    for lemon_id in range(1, 4982):
        lemon_page = LemonAmigaEntry(lemon_id)

        wikidata_game = database_claim(lemon_id, 'P4671', lemon_page.get_hol_id(), 'Hall of Light ID', u'P4846', 'Lemon Amiga ID.')
        if wikidata_game is not None:
            continue

        wikidata_game = database_claim(lemon_id, 'P11688', lemon_page.get_mobygames_id(), 'Mobygames numeric ID', u'P4846', 'Lemon Amiga ID.')
        if wikidata_game is not None:
            continue

        wikidata_game = database_claim(lemon_id, 'P4858', lemon_page.get_atarilegend_id(), 'Atari Legend ID', u'P4846', 'Lemon Amiga ID.')
        if wikidata_game is not None:
            continue

        wikidata_game = database_claim(lemon_id, 'P7555', lemon_page.get_uvl_id(), 'UVL game ID', u'P4846', 'Lemon Amiga ID.')
        if wikidata_game is not None:
            continue

        wikidata_game = database_claim(lemon_id, 'P7516', lemon_page.get_sps_id(), 'Software Preservation Society ID', u'P4846', 'Lemon Amiga ID.')
        if wikidata_game is not None:
            continue

        wikidata_game = database_claim(lemon_id, 'P12225', lemon_page.get_whdload_id(), 'WHDLoad database ID', u'P4846', 'Lemon Amiga ID.')
        if wikidata_game is not None:
            continue


class Lemon64Entry():

    def __init__(self, lemon_id):
        url = f"https://www.lemon64.com/?game_id={str(lemon_id)}"
        print(f"Processing URL {url}")
        try:
            request = urllib.request.Request(url, headers=headers)
            response = urllib.request.urlopen(request)
            html = response.read().decode("utf-8")
        except Exception as error:
            pywikibot.output(error)
            return

        self.html = html

    def get_mobygames_id(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        a = soup.find('a', title ='External link: More information on MobyGames')
        if a and 'href' in a.attrs:
            match = re.match(r"^https?://www\.mobygames\.com/game/(\d+).*$", a['href'])
            if match:
                return match.group(1)
        else:
            return None

    def get_wikipedia_slug(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        a = soup.find('a', title ='External link: More information on Wikipedia')
        if a and 'href' in a.attrs:
            return a['href'].split('/')[-1]
        else:
            return None

    def get_uvl_id(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        a = soup.find('a', title ='External link: More information on UVL')
        if a and 'href' in a.attrs:
            match = re.match(r"^https?://www\.uvlist\.net/game-(\d+).*$", a['href'])
            if match:
                return match.group(1)
        else:
            return None

    def get_gamebase64_id(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        a = soup.find('a', title ='External link: More information on Gamebase64')
        if a and 'href' in a.attrs:
            match = re.match(r"^https?://gb64\.com/game\.php\?id=(\d+)$", a['href'])
            if match:
                return match.group(1)
        else:
            return None

    def get_c64dotcom_id(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        a = soup.find('a', title ='External link: More information on C64.com')
        if a and 'href' in a.attrs:
            match = re.match(r"^https?://www\.c64\.com/games/(\d+)$", a['href'])
            if match:
                return match.group(1)
        else:
            return None

    def get_ready64_id(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        a = soup.find('a', title ='External link: More information on Ready64')
        if a and 'href' in a.attrs:
            match = re.match(r"^https?://ready64\.org/giochi/scheda_gioco/id/(\d+).*$", a['href'])
            if match:
                return match.group(1)
        else:
            return None


def match_lemon64():
    for lemon_id in range(1, 6873):
        lemon_page = Lemon64Entry(lemon_id)

        wikidata_game = database_claim(lemon_id, 'P11688', lemon_page.get_mobygames_id(), 'Mobygames numeric ID', u'P4816', 'Lemon 64 ID.')
        if wikidata_game is not None:
            continue

        wikidata_game = database_claim(lemon_id, 'P7555', lemon_page.get_uvl_id(), 'UVL game ID', u'P4816', 'Lemon 64 ID.')
        if wikidata_game is not None:
            continue

        wikidata_game = database_claim(lemon_id, 'P4917', lemon_page.get_gamebase64_id(), 'Gamebase64 ID', u'P4816', 'Lemon 64 ID.')
        if wikidata_game is not None:
            continue

        wikidata_game = database_claim(lemon_id, 'P7852', lemon_page.get_gamebase64_id(), 'C64.COM ID', u'P4816', 'Lemon 64 ID.')
        if wikidata_game is not None:
            continue

        wikidata_game = database_claim(lemon_id, 'P4916', lemon_page.get_ready64_id(), 'Ready64 ID', u'P4816', 'Lemon 64 ID.')
        if wikidata_game is not None:
            continue


#harvest_abandonia()
#harvest_c64wiki()
#match_lemonamiga()
match_lemon64()
