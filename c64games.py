#!/usr/bin/python3

import urllib.request
import re
import pywikibot
from pywikibot.data.sparql import SparqlQuery
from bs4 import BeautifulSoup

repo = pywikibot.Site()
repo.login()

class DatabaseEntry():

    def __init__(self, id):
        url = f"https://www.c64games.de/phpseiten/spieledetail.php?filnummer={str(id)}"
        print(f"Processing URL {url}")
        try:
            request = urllib.request.Request(url, None)
            response = urllib.request.urlopen(request)
            html = response.read().decode("ISO-8859-1")
        except Exception as error:
            pywikibot.output(error)
            return

        self.html = html

    def get_c64_wiki_id(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        a_tag_with_title = soup.find('a', string='C64-WIKI')
        if a_tag_with_title and 'href' in a_tag_with_title.attrs:
            return a_tag_with_title['href'].split('/')[-1]
        else:
            return None


def find_item_from_external_id(property, id):
    sparql = SparqlQuery()
    try:
        results = sparql.select(f"""
            SELECT ?item WHERE {{
            ?item wdt:{property} "{id}" .
            }}
        """)
    except Exception as error:
        pywikibot.error("Failed to query for {id}", error)
        return None

    if len(results) == 0:
        return None
    match = re.match(r"^https?://www\.wikidata\.org/entity/(Q\d+)$", results[0]["item"])
    if not match:
        return None
    return pywikibot.ItemPage(repo, match.group(1))


if __name__ == "__main__":
    for c64games_id in range(1, 7893):
        c64_games_page = DatabaseEntry(c64games_id)
        c64_wiki_id = c64_games_page.get_c64_wiki_id()
        if not c64_wiki_id is None:
            wikidata_game = find_item_from_external_id('P11129', c64_wiki_id)
            if wikidata_game is not None:
                print(f"Found matching Wikidata {wikidata_game}")

                claims = wikidata_game.get(u'claims')
                if u'P7853' in claims[u'claims']:
                    print(u'Skipping: Already has a C64Games.de ID!')
                    print()
                    continue
                else:
                    c64games_claim = pywikibot.Claim(repo, u'P7853')
                    c64games_claim.setTarget(str(c64games_id))
                    wikidata_game.addClaim(c64games_claim, summary=u'based on C64-Wiki ID.')
                    print(f"Adding C64Games.de ID.")
                    print()
            else:
                print(f"Error: no match in Wikidata.")
                print()
