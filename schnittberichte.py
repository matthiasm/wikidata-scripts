#!/usr/bin/python3

import urllib.request
import re
import pywikibot
from pywikibot.data.sparql import SparqlQuery
from bs4 import BeautifulSoup

repo = pywikibot.Site()
repo.login()

class DatabaseEntry():

    def __init__(self, title_id):
        url = f"https://www.schnittberichte.com/svds.php?Page=Titel&ID={str(title_id)}"
        pywikibot.output(f"Processing URL {url}")
        try:
            request = urllib.request.Request(url, None)
            response = urllib.request.urlopen(request)
            html = response.read().decode("ISO-8859-15")
        except Exception as error:
            pywikibot.output(error)
            return

        self.html = html

    def get_ogdb_id(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        anchor = soup.find('a', string=re.compile(r'OGDB'))
        if anchor is None:
            return None

        url = anchor['href']
        match = re.search(r'titleid=(\d+)', url)
        if match:
            return match.group(1)
        else:
            return None

    def get_ofdb_id(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        anchor = soup.find('a', string=re.compile(r'OFDB'))
        if anchor is None:
            return None

        url = anchor['href']
        match = re.search(r'film/(\d+),', url)
        if match:
            return match.group(1)
        else:
            return None

    def get_imdb_id(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        anchor = soup.find('a', string=re.compile(r'IMDB'))
        if anchor is None:
            return None

        url = anchor['href']
        match = re.search(r'title/(tt\d+)', url)
        if match:
            return match.group(1)
        else:
            return None


def find_item_from_ogdb_id(ogdb_id):
    sparql = SparqlQuery()
    try:
        result = sparql.select(f"""
            SELECT ?item WHERE {{
            ?item wdt:P7564 "{ogdb_id}" .
            }}
        """)
    except Exception as error:
        pywikibot.output(error)
        return None
    if len(result) != 1:
        return None
    match = re.match(r"^https?://www\.wikidata\.org/entity/(Q\d+)$", result[0]["item"])
    if not match:
        return None
    return pywikibot.ItemPage(repo, match.group(1))


def find_item_from_ofdb_id(ofdb_id):
    sparql = SparqlQuery()
    try:
        result = sparql.select(f"""
            SELECT ?item WHERE {{
            ?item wdt:P3138 "{ofdb_id}" .
            }}
        """)
    except Exception as error:
        pywikibot.output(error)
        return None
    if len(result) != 1:
        return None
    match = re.match(r"^https?://www\.wikidata\.org/entity/(Q\d+)$", result[0]["item"])
    if not match:
        return None
    return pywikibot.ItemPage(repo, match.group(1))


def find_item_from_imdb_id(imdb_id):
    sparql = SparqlQuery()
    try:
        result = sparql.select(f"""
            SELECT ?item WHERE {{
            ?item wdt:P345 "{imdb_id}" .
            }}
        """)
    except Exception as error:
        pywikibot.output(error)
        return None
    if len(result) != 1:
        return None
    match = re.match(r"^https?://www\.wikidata\.org/entity/(Q\d+)$", result[0]["item"])
    if not match:
        return None
    return pywikibot.ItemPage(repo, match.group(1))


if __name__ == "__main__":
    for title_id in range(1, 92730):
        schnittberichte_page = DatabaseEntry(title_id)

        ogdb_id = schnittberichte_page.get_ogdb_id()
        if not ogdb_id is None:
            wikidata_game = find_item_from_ogdb_id(ogdb_id)
            if wikidata_game is not None:
                print(f"Found matching {wikidata_game}")

                claims = wikidata_game.get(u'claims')
                if u'P12241' in claims[u'claims']:
                    print(u'Skipping: Already has a Schnittberichte title ID!')
                    print()
                    continue
                else:
                    schnittberichte_claim = pywikibot.Claim(repo, u'P12241')
                    schnittberichte_claim.setTarget(str(title_id))
                    wikidata_game.addClaim(schnittberichte_claim, summary=u'Add Schnittberichte title based on OGDB game title ID.')
                    print(f"Adding Schnittberichte title.")
                    print()
                    continue
            else:
                print(f"No match in Wikidata.")
                print()

        ofdb_id = schnittberichte_page.get_ofdb_id()
        if not ofdb_id is None:
            wikidata_movie = find_item_from_ofdb_id(ofdb_id)
            if wikidata_movie is not None:
                print(f"Found matching {wikidata_movie}")

                claims = wikidata_movie.get(u'claims')
                if u'P12241' in claims[u'claims']:
                    print(u'Skipping: Already has a Schnittberichte title ID!')
                    print()
                    continue
                else:
                    schnittberichte_claim = pywikibot.Claim(repo, u'P12241')
                    schnittberichte_claim.setTarget(str(title_id))
                    wikidata_movie.addClaim(schnittberichte_claim, summary=u'Add Schnittberichte title based on OFDb film ID.')
                    print(f"Adding Schnittberichte title.")
                    print()
                    continue
            else:
                print(f"No match in Wikidata.")

        imdb_id = schnittberichte_page.get_imdb_id()
        if not imdb_id is None:
            wikidata_movie = find_item_from_imdb_id(imdb_id)
            if wikidata_movie is not None:
                print(f"Found matching {wikidata_movie}")

                claims = wikidata_movie.get(u'claims')
                if u'P12241' in claims[u'claims']:
                    print(u'Skipping: Already has a Schnittberichte title ID!')
                    print()
                    continue
                else:
                    schnittberichte_claim = pywikibot.Claim(repo, u'P12241')
                    schnittberichte_claim.setTarget(str(title_id))
                    wikidata_movie.addClaim(schnittberichte_claim, summary=u'Add Schnittberichte title based on IMDb ID.')
                    print(f"Adding Schnittberichte title.")
                    print()
                    continue
            else:
                print(f"No match in Wikidata.")
                print()
