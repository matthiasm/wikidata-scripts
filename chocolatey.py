#!/usr/bin/python3

import re
import pywikibot
import time
from pywikibot.data.sparql import SparqlQuery
from webdriver_manager.chrome import ChromeDriverManager
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from urllib.parse import urlparse

repo = pywikibot.Site()
repo.login()

options = webdriver.ChromeOptions()
options.headless = True
options.page_load_strategy = 'none'
chrome_path = ChromeDriverManager().install()
chrome_service = Service(chrome_path)
driver = webdriver.Chrome(options=options, service=chrome_service)
driver.implicitly_wait(5)


def getPackages(page):
    url = f"https://community.chocolatey.org/packages?sortOrder=package-download-count&page={page}&prerelease=False&moderatorQueue=False&moderationStatus=all-statuses"
    print(f"Processing https://community.chocolatey.org/packages page {page}")
    try:
        driver.get(url)
        time.sleep(5)
        elements = driver.find_elements(By.XPATH, '//*[@id="package"]/div/div[2]/ul[2]/li/div[1]/div/div[2]/div[1]/a')
    except Exception as error:
        pywikibot.output(error)

    packages = []
    for element in elements:
        segments = element.get_attribute('href').split('/')
        if is_version(segments[-1]):
            segments = segments[:-1]
        packages.append(segments[-1])

    return packages


def is_version(segment):
    return bool(re.match(r'^\d+(\.\d+)*$', segment))


class ChocolateyPage():
    def __init__(self, url):
        print(f"Processing URL {url}")
        self.package_name = urlparse(url).path.split('/')[-1]
        try:
            driver.get(url)
            time.sleep(5)
            anchor_element = driver.find_element(By.XPATH, "//a[contains(@title, 'Nuspec reference: <projectUrl>')]")
            project_url = anchor_element.get_attribute('href')
        except NoSuchElementException:
            self.project_url = None
            print(f"Failed to find project URL from nuspec reference.")
            return
        except Exception as error:
            pywikibot.output(error)
            return

        print(f"Extracted project URL {project_url}")
        self.project_url = project_url.strip()


def find_item_from_official_website(url):
    print(f"Searching {url}")
    sparql = SparqlQuery()
    try:
        result = sparql.select(f"""
            SELECT ?item WHERE {{
                ?item wdt:P856 <{url}> .
            }}
        """)
    except Exception as error:
        print(error)
        return None
    if len(result) != 1:
        return None
    match = re.match(r"^https?://www\.wikidata\.org/entity/(Q\d+)$", result[0]["item"])
    if not match:
        return None
    return pywikibot.ItemPage(repo, match.group(1))


if __name__ == "__main__":
        for page in range(0, 329):
            for package in getPackages(page):

                # Windows Updates
                if package.startswith('KB'):
                    continue

                choco_page = ChocolateyPage(f"https://community.chocolatey.org/packages/{package}")
                if not choco_page.package_name is None and not choco_page.project_url is None:
                    url = choco_page.project_url.replace("http://", "https://")
                    entity = find_item_from_official_website(url)
                    if entity is None:
                        if url.endswith("/"):
                            entity = find_item_from_official_website(url[:-1])
                        else:
                            entity = find_item_from_official_website(url + "/")
                    if entity is None:
                        if "://www." in url:
                            url = url.replace("://www.", "://")
                            entity = find_item_from_official_website(url)
                            if entity is None:
                                if url.endswith("/"):
                                    entity = find_item_from_official_website(url[:-1])
                                else:
                                    entity = find_item_from_official_website(url + "/")
                        else:
                            url = url.replace("://", "://www.")
                            entity = find_item_from_official_website(url)
                            if entity is None:
                                if url.endswith("/"):
                                    entity = find_item_from_official_website(url[:-1])
                                else:
                                    entity = find_item_from_official_website(url + "/")

                    if entity is not None:
                        print(f"Found matching Wikidata {entity}")

                        claims = entity.get(u'claims')
                        if u'P10766' in claims[u'claims']:
                            print(u'Already has a Chocolatey Community package ID!')
                        else:
                            claim = pywikibot.Claim(repo, u'P10766')
                            claim.setTarget(choco_page.package_name)
                            entity.addClaim(claim, summary=u'Add Chocolatey Community package ID based on project URL.')
                            print(f"Adding Chocolatey Community package ID ")
                    else:
                        print(f"No match in Wikidata.")

                    print()

            print()
