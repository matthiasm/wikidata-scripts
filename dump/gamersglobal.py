#!/usr/bin/python3

import pywikibot
import urllib.request
from bs4 import BeautifulSoup
import re

def get_games(page):
    url = f"https://www.gamersglobal.de/spiele/datum?page={str(page)}"
    print(f"Processing URL {url}")
    try:
        request = urllib.request.Request(url, None)
        response = urllib.request.urlopen(request)
        html = response.read().decode("utf-8")
    except Exception as error:
        pywikibot.output(error)

    soup = BeautifulSoup(html, 'lxml')
    for td in soup.findAll('td', class_='views-field views-field-title'):
        a = td.find('a')
        id = a['href'].split('/')[2]
        yield id

def write_game(id):
    url = f"https://www.gamersglobal.de/spiel/{str(id)}"
    print(f"Processing URL {url}")
    try:
        request = urllib.request.Request(url, None)
        response = urllib.request.urlopen(request)
        html = response.read().decode("utf-8")
    except Exception as error:
        pywikibot.output(error)
        return

    soup = BeautifulSoup(html, 'lxml')

    div = soup.find('div', class_='sb-title-content')
    title = div.text.strip()

    span = soup.find('span', class_='date-display-single')

    year = None
    description = None
    if span is not None:
        year = extract_year(span.text)
        if year is not None:
            description = year + " video game"

    if year is None:
        description = "video game"

    with open("output.tsv", "a") as file:
        file.write(f"{id}\t{title}\t{description}\t{url}\t\t\t\t\t\tQ7889\n")

def extract_year(date):
    match = re.search(r'(\d{4})', date)
    if match:
        return match.group(1)
    else:
        return None

if __name__ == "__main__":
    with open("output.tsv", "w") as file:
        file.write("ID\tname\tDescription\tURL\tP11771\tq\tautoq\tcoord\t\ttype\n")
    for page in range(2409, 2410):
        for game in get_games(page):
            write_game(game)
