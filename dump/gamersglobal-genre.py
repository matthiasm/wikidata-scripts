#!/usr/bin/python3

import csv
import time
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from bs4 import BeautifulSoup

options = webdriver.ChromeOptions()
options.headless = True
options.page_load_strategy = 'none'
driver = webdriver.Chrome(options=options)


class GamersGlobal:

    def login(self):
        driver.get("https://www.gamersglobal.de/user/login/")
        time.sleep(2)

        driver.find_element(By.NAME, "name").send_keys(username)
        driver.find_element(By.NAME, "pass").send_keys(password)
        driver.find_element(By.NAME, "pass").send_keys(Keys.RETURN)
        time.sleep(2)

        if "logout" in driver.page_source.lower():
            print("Logged in successfully!")
        else:
            print("Login failed.")
            driver.quit()

    def close(self):
        driver.quit()


gamers_global = GamersGlobal()
gamers_global.login()

driver.get('https://www.gamersglobal.de/spiele')
time.sleep(5)
html = driver.page_source

soup = BeautifulSoup(html, 'html.parser')
select_element = soup.find('select', {"name": "field_genre_tags_value"})

with open("output.tsv", mode='w', newline='') as file:
    writer = csv.writer(file, delimiter='\t')
    writer.writerow(["ID", "name", "Description", "URL", "P13120", "q", "autoq", "coord", "", "type"])

    for option in select_element.find_all('option'):
        id = option["value"]
        if id == 'All':
            continue

        genre = option.text
        if genre.startswith('-'):
            genre = genre.lstrip('-')

        if genre == 'Hardware':
            continue
        if genre == 'Konsole':
            continue
        if genre == 'Handheld':
            continue
        if genre == 'Homecomputer':
            continue

        writer.writerow([id, genre, "", 'https://www.gamersglobal.de/spiele?field_genre_tags_value='+id])

driver.close()
