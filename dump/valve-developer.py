#!/usr/bin/python3

import csv
import requests

url = "https://developer.valvesoftware.com/w/api.php"
params = {
    "action": "query",
    "format": "json",
    "list": "allpages",
    "aplimit": "max"
}

all_pages = []

while True:
    response = requests.get(url, params=params)
    data = response.json()
    pages = data["query"]["allpages"]
    all_pages.extend(pages)

    if "continue" in data:
        params["apcontinue"] = data["continue"]["apcontinue"]
    else:
        break

with open("output.tsv", mode='w', newline='') as file:
    writer = csv.writer(file, delimiter='\t')
    writer.writerow(["ID", "name", "Description", "URL", "P12093", "q", "autoq", "coord", "", "type" ])

    for page in all_pages:
        id = page["title"].replace(" ", "_")
        writer.writerow([id, page["title"], "", "https://developer.valvesoftware.com/wiki/"+id])