from googlesearch import search
import csv
import requests
import re

def get_title(publication_id):
    publication_url = f"https://www.metacritic.com/publication/{publication_id}/"
    print(f"Processing {publication_url}")
    headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3'}
    response = requests.get(publication_url, headers=headers)

    if response.status_code == 200:
        page_content = response.text
    else:
        print(f"Error {response.status_code}")
        return None

    title_regex = r'<h1 class="c-globalHeader_titleText">\s*(.*?)\s*</h1>'
    match = re.search(title_regex, page_content)
    if match:
        return match.group(1)

if __name__ == "__main__":
    query = "site:metacritic.com/publication/"
    publication_ids = set()

    for url in search(query, num_results=1000):
        match = re.search(r'metacritic\.com/publication/([\w-]+)', url)
        if match:
            publication_ids.add(match.group(1))

with open("output.tsv", mode='w', newline='') as file:
    writer = csv.writer(file, delimiter='\t')
    writer.writerow(["ID", "name", "Description", "URL", "P12079", "q", "autoq", "coord", "", "type" ])

    for publication_id in publication_ids:
        title = get_title(publication_id)
        if title is None:
            continue

        writer.writerow([publication_id, title, "", "https://metacritic.com/publication/"+publication_id])