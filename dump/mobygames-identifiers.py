#!/usr/bin/python3

import re
import csv
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from bs4 import BeautifulSoup


options = webdriver.ChromeOptions()
options.headless = True
options.page_load_strategy = 'none'
driver = webdriver.Chrome(options=options)
driver.implicitly_wait(5)


class MobyGames:

    def login(self):
        driver.get("https://www.mobygames.com/user/login/")
        time.sleep(2)

        driver.find_element(By.NAME, "login").send_keys(e-Mail)
        driver.find_element(By.NAME, "password").send_keys(password)
        driver.find_element(By.NAME, "password").send_keys(Keys.RETURN)
        time.sleep(2)

        if "logout" in driver.page_source.lower():
            print("Logged in successfully!")
        else:
            print("Login failed.")
            driver.quit()

    def close(self):
        driver.quit()


class GamePage:

    def __init__(self, mobygames_id):
        url = f"https://www.mobygames.com/game/{str(mobygames_id)}"
        print(f"Processing URL {url}")
        driver.get(url)
        time.sleep(2)
        self.html = driver.page_source


def get_steam_id(self):
    if not hasattr(self, 'html'):
        return None

    soup = BeautifulSoup(self.html, 'lxml')
    section = soup.find('section', id='gameIdentifiers')
    if not section:
        return None

    steam_link = section.find('a', href=re.compile(r'store\.steampowered\.com/app/(\d+)/'))
    if steam_link:
        return re.search(r'/app/(\d+)/', steam_link['href']).group(1)


def get_itch_id(self):
    if not hasattr(self, 'html'):
        return None

    soup = BeautifulSoup(self.html, 'lxml')
    section = soup.find('section', id='gameIdentifiers')
    if not section:
        return None

    match = re.search(r'Itch\.io:\s(\d+)', str(section))
    return match.group(1) if match else None


def get_wikipedia_article(self):
    if not hasattr(self, 'html'):
        return None

    soup = BeautifulSoup(self.html, 'lxml')
    section = soup.find('section', id='gameIdentifiers')
    if not section:
        return None

    wikipedia_article = section.find('a', href=re.compile(r'en\.wikipedia\.org/wiki/(.+)'))
    if wikipedia_article:
        return re.search(r'/wiki/(.+)', wikipedia_article['href']).group(1)


def get_gog_slug(self):
    if not hasattr(self, 'html'):
        return None

    soup = BeautifulSoup(self.html, 'lxml')
    section = soup.find('section', id='gameIdentifiers')
    if not section:
        return None

    gog_link = section.find('a', href=re.compile(r'www\.gog\.com/game/(.+)'))
    if gog_link:
        return re.search(r'/game/(.+)', gog_link['href']).group(1)


def get_gog_sku(self):
    if not hasattr(self, 'html'):
        return None

    soup = BeautifulSoup(self.html, 'lxml')
    section = soup.find('section', id='gameIdentifiers')
    if not section:
        return None

    match = re.search(r'GOG\.com SKU:\s(\d+)', str(section))
    return match.group(1) if match else None


if __name__ == "__main__":

    moby_games = MobyGames()
    moby_games.login()

    with open("output.csv", mode='w', newline='') as file:
        writer = csv.writer(file, delimiter=';')
        writer.writerow(["MobyGames", "Steam", "itch.io", "Wikipedia", "GOG slug", "GOG product"])

        for mobygames_id in range(232000, 1, -1):
            game_page = GamePage(mobygames_id)
            writer.writerow([mobygames_id, get_steam_id(game_page), get_itch_id(game_page),
                            get_wikipedia_article(game_page), get_gog_slug(game_page), get_gog_sku(game_page)])

    moby_games.close()
