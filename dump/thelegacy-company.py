#!/usr/bin/python3

from bs4 import BeautifulSoup
import csv
import requests
import urllib.parse
import re


def fetch_wayback(company_id):
    url = urllib.parse.quote(f"www.thelegacy.de/Museum/SQLlist_games.php3?misc=yes&company_id={company_id}", safe='')
    api_url = f"https://archive.org/wayback/available?url={url}&status_code=200&timeout=60"
    try:
        response = requests.get(api_url)
        data = response.json()

        if 'archived_snapshots' in data and 'closest' in data['archived_snapshots']:
            snapshot = data['archived_snapshots']['closest']
            archive_url = snapshot['url']
            print(f"Found archived URL: {archive_url}")

            archived_html = requests.get(archive_url)
            return archived_html.text, archive_url
        else:
            print(f"{url} was not archived.")
            return None, None
    except Exception as error:
        print(error)
        return None, None


def get_company_name(html_content):
    soup = BeautifulSoup(html_content, 'html.parser')
    descriptions = soup.find_all('div', class_='description')
    for description in descriptions:
        match = re.match(r"^[^()]+", description.get_text())
        if match:
            return match.group().strip()


with open("output.tsv", mode='w', newline='') as file:
    writer = csv.writer(file, delimiter='\t')
    writer.writerow(["ID", "name", "Description", "URL", "P12734", "q", "autoq", "coord", "", "type"])

    for company_id in range(1, 10000):
        html_content, archive_url = fetch_wayback(company_id)
        if html_content:
            company_name = get_company_name(html_content)
            print(f"Found company name {company_name}.")
            writer.writerow([company_id, company_name, "", archive_url])

        print()
