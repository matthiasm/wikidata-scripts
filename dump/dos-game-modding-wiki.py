#!/usr/bin/python3

import csv
import requests

url = "https://moddingwiki.shikadi.net/w/api.php"
params = {
    "action": "query",
    "format": "json",
    "list": "categorymembers",
    "cmtitle": "Category:Game Intro Page",
    "cmlimit": "max"
}

category_pages = []

while True:
    response = requests.get(url, params=params)
    data = response.json()
    pages = data["query"]["categorymembers"]
    category_pages.extend(pages)

    if "continue" in data:
        params["cmcontinue"] = data["continue"]["cmcontinue"]
    else:
        break

with open("output.tsv", mode='w', newline='') as file:
    writer = csv.writer(file, delimiter='\t')
    writer.writerow(["ID", "name", "Description", "URL", "P12960", "q", "autoq", "coord", "", "type" ])

    for page in category_pages:
        id = page["title"].replace(" ", "_")
        writer.writerow([id, page["title"], "", "https://moddingwiki.shikadi.net/wiki/"+id])
