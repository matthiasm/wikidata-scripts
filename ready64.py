#!/usr/bin/python3

import re
import pywikibot
from pywikibot.data.sparql import SparqlQuery

site = pywikibot.Site(url='https://www.c64-wiki.de', fam='c64')

repo = pywikibot.Site()
repo.login()


def find_item_from_external_id(property, id):
    sparql = SparqlQuery()
    try:
        results = sparql.select(f"""
            SELECT ?item WHERE {{
            ?item wdt:{property} "{str(id)}" .
            }}
        """)
    except Exception as error:
        pywikibot.error("Failed to query for {id}", error)
        return None

    if results is None:
        return None

    if len(results) == 0:
        return None
    match = re.match(r"^https?://www\.wikidata\.org/entity/(Q\d+)$", results[0]["item"])
    if not match:
        return None
    return pywikibot.ItemPage(repo, match.group(1))


def extract_template_id(source_content):
    match = re.search(r"{{ready64\|(\d+)}}", source_content)
    if match:
        return match.group(1)
    else:
        return None


def claim(wikidata_game, property, id, name, base):
    claims = wikidata_game.get(u'claims')
    if property in claims[u'claims']:
        print(f'Skipping: Already has a {name} ID!')
    else:
        claim = pywikibot.Claim(repo, property)
        claim.setTarget(str(id))
        wikidata_game.addClaim(claim, summary=f'based on {base} ID.')
        print(f"Adding {name} ID.")


def harvest_c64wiki():
    category = pywikibot.Category(site, 'Kategorie:Spiel')
    for page in list(category.articles()):
        if page.title() == "C64-Wiki:Musterartikel (Spiel)":
            continue

        if page.title() == "Testartikel (Spielvoting)":
            continue

        if "Übungsartikel" in page.title():
            continue

        print(page.title())

        ready64_id = extract_template_id(page.text)
        if ready64_id is None:
            continue

        wikidata_game = find_item_from_external_id('P11129', str(page.title().replace(' ', '_')))
        if wikidata_game is not None:
            print(f"Found matching Wikidata {wikidata_game}")
            claim(wikidata_game, u'P4916', ready64_id, 'Ready64 ID', 'C64-Wiki')
        else:
            print(f"No match in Wikidata.")

        print()


harvest_c64wiki()
