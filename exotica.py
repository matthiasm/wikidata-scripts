#!/usr/bin/python3

import pywikibot
import re
from pywikibot.data.sparql import SparqlQuery

site = pywikibot.Site(url='https://www.exotica.org.uk/wiki/', fam='exotica')

repo = pywikibot.Site()
repo.login()


def fetch_games_pages():
    category = pywikibot.Category(site, 'Category:Amiga Games')
    pages = list(category.articles())
    return pages


def extract_template_id(source_content, database):
    match = re.search(rf"{{{database}\|(\d+)\|", source_content)
    if match:
        return match.group(1)
    else:
        return None


def find_item_from_external_id(property, id):
    sparql = SparqlQuery()
    try:
        results = sparql.select(f"""
            SELECT ?item WHERE {{
            ?item wdt:{property} "{id}" .
            }}
        """)
    except Exception as error:
        pywikibot.error("Failed to query for {id}", error)
        return None

    if len(results) == 0:
        return None
    match = re.match(r"^https?://www\.wikidata\.org/entity/(Q\d+)$", results[0]["item"])
    if not match:
        return None
    return pywikibot.ItemPage(repo, match.group(1))


def database_claim(page, property, template_id, identifier_name):
    wikidata_game = find_item_from_external_id(property, extract_template_id(page.text, template_id))
    if wikidata_game is not None:
        print(f"Found matching Wikidata {wikidata_game}")

        claims = wikidata_game.get(u'claims')
        if u'P7981' in claims[u'claims']:
            print(u'Skipping: Already has an ExoticA ID!')
        else:
            c64wiki_claim = pywikibot.Claim(repo, u'P7981')
            c64wiki_claim.setTarget(str(page.title().replace(' ', '_')))
            wikidata_game.addClaim(c64wiki_claim, summary=u'based on ' + identifier_name)
            print(f"Adding ExoticA ID.")

        print()
        return wikidata_game

    return None


def match_game_databases():
    for page in fetch_games_pages():
        print(page.title())

        wikidata_game = database_claim(page, 'P4671', 'hol' ,'Hall of Light ID')
        if wikidata_game is not None:
            continue

        wikidata_game = database_claim(page, 'P4846', 'lemon' ,'Lemon Amiga ID')
        if wikidata_game is not None:
            continue

        print()


match_game_databases()
