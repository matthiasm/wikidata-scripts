#!/usr/bin/python3

import urllib.request
import re
import pywikibot
from pywikibot.data.sparql import SparqlQuery
from bs4 import BeautifulSoup

repo = pywikibot.Site()
repo.login()

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3'
}

class LemonAmigaEntry():

    def __init__(self, lemon_id):
        url = f"https://www.lemonamiga.com/games/details.php?id={str(lemon_id)}"
        print(f"Processing URL {url}")
        try:
            request = urllib.request.Request(url, headers=headers)
            response = urllib.request.urlopen(request)
            html = response.read().decode("utf-8")
        except Exception as error:
            pywikibot.output(error)
            return

        self.html = html

    def get_sps_id(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        a = soup.find('a', title ='External link: Software Preservation Society')
        if a and 'href' in a.attrs:
            match = re.match(r"^https?://www\.softpres\.org/\?id=:games&gameid=(\d+)$", a['href'])
            if match:
                return match.group(1)
        else:
            return None


def find_item_from_external_id(property, id):
    sparql = SparqlQuery()
    try:
        results = sparql.select(f"""
            SELECT ?item WHERE {{
            ?item wdt:{property} "{str(id)}" .
            }}
        """)
    except Exception as error:
        pywikibot.error("Failed to query for {id}", error)
        return None

    if results is None:
        return None

    if len(results) == 0:
        return None

    match = re.match(r"^https?://www\.wikidata\.org/entity/(Q\d+)$", results[0]["item"])
    if not match:
        return None

    return pywikibot.ItemPage(repo, match.group(1))


def claim(wikidata_game, property, id, base):
    claims = wikidata_game.get(u'claims')
    if property in claims[u'claims']:
        print(f'Skipping: Already has a oftware Preservation Society ID!')
    else:
        claim = pywikibot.Claim(repo, property)
        claim.setTarget(str(id))
        wikidata_game.addClaim(claim, summary=f'based on {base}.')
        print(f"Adding Software Preservation Society ID.")


def harvest_lemonamiga():
    for lemon_id in range(1, 4982):
        lemon_page = LemonAmigaEntry(lemon_id)
        sps_id = lemon_page.get_sps_id()
        if sps_id is None:
            continue

        wikidata_game = find_item_from_external_id('P4846', lemon_id)
        if wikidata_game is not None:
            print(f"Found matching Wikidata {wikidata_game}")
            claim(wikidata_game, u'P7516', sps_id, 'Lemon Amiga ID')
        else:
            print(f"No match in Wikidata.")

        print()

harvest_lemonamiga()
