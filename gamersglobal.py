#!/usr/bin/python3

import pywikibot
import urllib.request
from bs4 import BeautifulSoup
import re
from pywikibot.data.sparql import SparqlQuery
from urllib.parse import urlparse, urlunparse

repo = pywikibot.Site()
repo.login()


def get_website_url(gamersglobal_game_id):
    url = f"https://www.gamersglobal.de/spiel/{gamersglobal_game_id}/"
    pywikibot.output(f"Processing URL {url}")
    try:
        request = urllib.request.Request(url, None)
        response = urllib.request.urlopen(request)
        html_reponse = response.read().decode("utf-8")
    except Exception as error:
        pywikibot.stdout(error)
        return None

    try:
        soup = BeautifulSoup(html_reponse, 'lxml')
        strong_tag = soup.find('strong', string='Offizielle Website:')
        if strong_tag:
            url_tag = strong_tag.find_next('a', href=True)
            if url_tag:
                return url_tag['href']
    except Exception as error:
        pywikibot.stdout(error)
        return None


def find_item_from_official_website(url):
    variations = set()

    parsed_url = urlparse(url)
    netloc = parsed_url.netloc
    path = parsed_url.path

    www_netloc = 'www.' + netloc if not netloc.startswith('www.') else netloc
    non_www_netloc = netloc[4:] if netloc.startswith('www.') else netloc

    for current_scheme in ['http', 'https']:
        for current_netloc in [www_netloc, non_www_netloc]:
            base_url = urlunparse((current_scheme, current_netloc, path, '', '', ''))

            variations.add(base_url if base_url.endswith('/') else base_url + '/')
            variations.add(base_url[:-1] if base_url.endswith('/') else base_url)

    for variation in variations:
        print(f"Searching website {variation}")
        sparql = SparqlQuery()
        try:
            result = sparql.select(f"""
                SELECT ?item WHERE {{
                    ?item wdt:P856 <{variation}> .
                }}
            """)
        except Exception as error:
            pywikibot.output(error)
            continue
        if result is None:
            continue
        if len(result) != 1:
            continue
        match = re.match(r"^https?://www\.wikidata\.org/entity/(Q\d+)$", result[0]["item"])
        if not match:
            return None
        return pywikibot.ItemPage(repo, match.group(1))
    return None


def is_videogame(wikidata_entity):
    claims = wikidata_entity.get(u'claims')
    if u'P31' not in claims[u'claims']:
        print("Unknown instance.")
        return False

    video_game_qids = [7889, 16070115, 209163, 1066707, 865493]
    for claim in wikidata_entity.claims['P31']:
        instance_of_qid = claim.toJSON()['mainsnak']['datavalue']['value']['numeric-id']
        if instance_of_qid in video_game_qids:
            return True

    return False


if __name__ == "__main__":
    for gamersglobal_game_id in range(1,300000):
        url = get_website_url(gamersglobal_game_id)
        if url is None:
            print("No website found.")
            continue

        wikidata_entity = find_item_from_official_website(url)
        if wikidata_entity is None:
            print(f"No match in Wikidata.")
            continue

        if not is_videogame(wikidata_entity):
            print("Not a video game.")
            continue

        if wikidata_entity is not None:
            print(f"Found matching Wikidata {wikidata_entity}")

            claims = wikidata_entity.get(u'claims')
            if u'P11771' in claims[u'claims']:
                print(u'Already has a GamersGlobal game ID!')
            else:
                claim = pywikibot.Claim(repo, u'P11771')
                claim.setTarget(str(gamersglobal_game_id))
                wikidata_entity.addClaim(claim, summary=u'Add GamersGlobal game ID based on official website.')
                print(f"Adding GamersGlobal game ID")

        print()
