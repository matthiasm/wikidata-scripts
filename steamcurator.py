#!/usr/bin/python3

import time
import urllib.request
import re
import pywikibot
from lxml import html
from urllib.parse import urlparse, parse_qs
from pywikibot.data.sparql import SparqlQuery
from webdriver_manager.chrome import ChromeDriverManager
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from bs4 import BeautifulSoup

repo = pywikibot.Site()
repo.login()

options = webdriver.ChromeOptions()
options.add_argument('--headless')
options.page_load_strategy = 'none'
chrome_path = ChromeDriverManager().install()
chrome_service = Service(chrome_path)
driver = webdriver.Chrome(options=options, service=chrome_service)
driver.implicitly_wait(5)

class SteamCuratorPage():

    def __init__(self, curator_id):
        url = f"https://store.steampowered.com/curator/{str(curator_id)}"
        print(f"Processing URL {url}")
        try:
            request = urllib.request.Request(url, None)
            response = urllib.request.urlopen(request)
            html = response.read().decode("utf-8")
        except Exception as error:
            pywikibot.output(error)
            return

        self.html = html


def get_title(self):
    if not hasattr(self, 'html'):
        return None

    soup = BeautifulSoup(self.html, 'lxml')
    h2 = soup.find('h2', class_='pageheader curator_name')
    if h2:
        return h2.a.text.strip()

    return None


def get_social(self, url):
    if not hasattr(self, 'html'):
        return None

    soup = BeautifulSoup(self.html, 'lxml')
    a_tag = soup.find('a', href=lambda x: x and url in x)
    if a_tag:
        social_link = a_tag['href']
        social_id = social_link.strip("/").split('/')[-1]
        if social_id is not None:
            print(f"Extracted {url} handle {social_id}")
            return social_id

    print(f"No {url} handle found.")
    return None


def get_website_url(self):
    if not hasattr(self, 'html'):
        return None

    try:
        soup = BeautifulSoup(self.html, 'lxml')
        tree = html.fromstring(str(soup))

        header_curator_details = tree.xpath('//*[@id="header_curator_details"]/div[1]/span/a/@href')
        if not header_curator_details:
            return None

        filtered_url = header_curator_details[0]
        parsed_url = urlparse(filtered_url)
        return parse_qs(parsed_url.query)['url'][0]
    except Exception:
        return None


def find_item_from_official_website(url):
    pywikibot.output(f"Searching {url}")
    sparql = SparqlQuery()
    try:
        result = sparql.select(f"""
            SELECT ?item WHERE {{
                ?item wdt:P856 <{url}> .
            }}
        """)
    except Exception as error:
        pywikibot.output(error)
        return None
    if result is None:
        return None
    if len(result) != 1:
        return None
    match = re.match(r"^https?://www\.wikidata\.org/entity/(Q\d+)$", result[0]["item"])
    if not match:
        return None
    return pywikibot.ItemPage(repo, match.group(1))


def find_item(handle, property):
    pywikibot.output("Query handle " + handle)
    sparql = SparqlQuery()
    try:
        result = sparql.select(f"""
            SELECT ?item WHERE {{
                ?item wdt:{property} ?handleValue .
                FILTER (lcase(?handleValue) = lcase("{handle}"))
            }}
        """)
    except Exception as error:
        pywikibot.output(error)
        return None
    if len(result) != 1:
        return None

    match = re.match(r"^https?://www\.wikidata\.org/entity/(Q\d+)$", result[0]["item"])
    if not match:
        return None

    return pywikibot.ItemPage(repo, match.group(1))


def extract_ids_from_elements(elements):
    """Extract IDs from elements and return as a set."""
    ids = set()
    for element in elements:
        url = element.get_attribute('href')
        match = re.search(r'/curator/(\d+)-', url)
        if match:
            ids.add(match.group(1))
    return ids


def get_clanid_from_developer(developer):
    url = f"https://store.steampowered.com/developer/{developer}"
    print(f"Processing URL {url}")
    try:
        request = urllib.request.Request(url, None)
        response = urllib.request.urlopen(request)
        html = response.read().decode("utf-8")
    except Exception as error:
        pywikibot.output(error)
        return

    soup = BeautifulSoup(html, 'html.parser')
    recommendation_rows = soup.select('#RecommendationsRows div a')
    if recommendation_rows:
        href = recommendation_rows[0].get('href')
        pattern = r'curator_clanid=(\d+)'
        match = re.search(pattern, href)
        if match:
            return match.group(1)
        else:
            print("No curator clan id found.")
            return None
    else:
        print("No recommendation rows found.")
        return None


if __name__ == "__main__":
    driver.get(f"https://store.steampowered.com/developer")
    driver.find_element(By.XPATH, '//*[@id="acceptAllButton"]/span').click()

    for page in range(1,333):
        print("Skipping page " + str(page))
        driver.find_element(By.XPATH, '//*[@id="Recommendations_btn_next"]').click()
        time.sleep(2)

    for page in range(333,479):
        print("Moving to page " + str(page))

        recommendation_rows = driver.find_elements(By.XPATH, '//*[@id="RecommendationsRows"]/div[*]/div[*]/div[1]/a')
        for recommendation_row in recommendation_rows:
            link = recommendation_row.get_attribute('href')
            if "developer" in link:
                steam_curator_id = get_clanid_from_developer(link.split("/developer/")[1].split("/")[0])
            elif "curator" in link:
                steam_curator_id = link.split("/curator/")[1].split("/")[0]

            if steam_curator_id is not None:
                curator_page = SteamCuratorPage(steam_curator_id)
                title = get_title(curator_page)
                if not title:
                    print("Skipping empty page.")
                    continue
                print(f"Found {title}")

                wikidata_entity = None
                twitch = get_social(curator_page, "https://www.twitch.tv/")
                if twitch is not None:
                    wikidata_entity = find_item(twitch, "P5797")

                if wikidata_entity is None:
                    youtube = get_social(curator_page, "https://www.youtube.com/")
                    if youtube is not None:
                        wikidata_entity = find_item(youtube, "P11245")

                if wikidata_entity is None:
                    twitter = get_social(curator_page, "https://twitter.com/")
                    if twitter is not None:
                        wikidata_entity = find_item(twitter, "P2002")

                if wikidata_entity is None:
                    facebook = get_social(curator_page, "https://www.facebook.com/")
                    if facebook is not None:
                        wikidata_entity = find_item(facebook, "P2013")

                if wikidata_entity is None:
                    website_url = get_website_url(curator_page)
                    if website_url is not None:
                        url = get_website_url(curator_page).replace("http://", "https://").lower()
                        wikidata_entity = find_item_from_official_website(url)
                        if wikidata_entity is None:
                            if url.endswith("/"):
                                wikidata_entity = find_item_from_official_website(url[:-1])
                            else:
                                wikidata_entity = find_item_from_official_website(url + "/")
                        if wikidata_entity is None:
                            if "://www." in url:
                                url = url.replace("://www.", "://")
                                wikidata_entity = find_item_from_official_website(url)
                                if wikidata_entity is None:
                                    if url.endswith("/"):
                                        wikidata_entity = find_item_from_official_website(url[:-1])
                                    else:
                                        wikidata_entity = find_item_from_official_website(url + "/")
                            else:
                                url = url.replace("://", "://www.")
                                wikidata_entity = find_item_from_official_website(url)
                                if wikidata_entity is None:
                                    if url.endswith("/"):
                                        wikidata_entity = find_item_from_official_website(url[:-1])
                                    else:
                                        wikidata_entity = find_item_from_official_website(url + "/")

                if wikidata_entity is not None:
                    print(f"Found matching Wikidata {wikidata_entity}")

                    claims = wikidata_entity.get(u'claims')
                    if u'P11975' in claims[u'claims']:
                        print(u'Already has a Steam curator ID!')
                    else:
                        claim = pywikibot.Claim(repo, u'P11975')
                        claim.setTarget(str(steam_curator_id))
                        wikidata_entity.addClaim(claim, summary=u'Add Steam curator ID based on social media handle.')
                        print(f"Adding Steam curator ID")
                else:
                    print(f"No match in Wikidata.")

                print()

        print()

        driver.find_element(By.XPATH, '//*[@id="Recommendations_btn_next"]').click()
        time.sleep(5)
