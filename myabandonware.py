#!/usr/bin/python3

import urllib.request
import re
import pywikibot
import requests
import string
import itertools
from pywikibot.data.sparql import SparqlQuery
from bs4 import BeautifulSoup

repo = pywikibot.Site()
repo.login()

class DatabaseEntry():

    def __init__(self, my_abandonware_id):
        url = f"https://www.myabandonware.com/game/--{str(my_abandonware_id)}"
        pywikibot.output(f"Processing URL {url}")
        try:
            request = urllib.request.Request(url, None)
            response = urllib.request.urlopen(request)
            html = response.read().decode("utf-8")
        except Exception as error:
            pywikibot.output(error)
            return

        self.html = html


    def get_external_id(self, label, pattern):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        a = soup.find('a', string=label)
        if a is None:
            return None
        match = re.match(pattern, a['href'])
        if match:
            return match.group(1)
        else:
            return None


def find_item_from_external_id(property, id):
    sparql = SparqlQuery()
    try:
        results = sparql.select(f"""
            SELECT ?item WHERE {{
            ?item wdt:{property} "{id}" .
            }}
        """)
    except Exception as error:
        pywikibot.error("Failed to query for {id}", error)
        return None

    if len(results) == 0:
        return None
    match = re.match(r"^https?://www\.wikidata\.org/entity/(Q\d+)$", results[0]["item"])
    if not match:
        return None
    return pywikibot.ItemPage(repo, match.group(1))


def get_wikidata_item_from_wikipedia_slug(title):
    url = "https://en.wikipedia.org/w/api.php"

    params = {
        'action': 'query',
        'prop': 'pageprops',
        'titles': title,
        'format': 'json'
    }

    response = requests.get(url, params=params)
    data = response.json()
    pages = data.get('query', {}).get('pages', {})
    if pages:
        for page_id in pages:
            page = pages[page_id]
            wikibase_item = page.get('pageprops', {}).get('wikibase_item')
            if wikibase_item is not None:
                return pywikibot.ItemPage(repo, wikibase_item)

    return None


def generate_patterns(start=None):
    characters = string.ascii_lowercase + string.digits
    all_patterns = []
    found_start = start is None

    for length in range(1, 4):
        for combo in itertools.product(characters, repeat=length):
            pattern = ''.join(combo)
            if not found_start:
                if pattern == start:
                    found_start = True
                continue
            all_patterns.append(pattern)

    return all_patterns


if __name__ == "__main__":
    for my_abandonware_id in generate_patterns():
        my_abandonware_page = DatabaseEntry(my_abandonware_id)

        moddb_id = my_abandonware_page.get_external_id('Mod DB', r'^https?:\/\/www\.moddb\.com\/games\/(\w+(?:\-\w+)*)')
        if not moddb_id is None:
            wikidata_game = find_item_from_external_id('P6774', moddb_id)
            if wikidata_game is not None:
                print(f"Found matching Wikidata {wikidata_game}")

                claims = wikidata_game.get(u'claims')
                if u'P12652' in claims[u'claims']:
                    print(u'Skipping: Already has a MyAbandonware game ID!')
                else:
                    my_abandonware_claim = pywikibot.Claim(repo, u'P12652')
                    my_abandonware_claim.setTarget(str(my_abandonware_id))
                    wikidata_game.addClaim(my_abandonware_claim, summary=u'based on ModDB game ID.')
                    print(f"Adding MyAbandonware game ID.")

                print()
                continue
            else:
                print(f"No match in Wikidata.")

        pcgamingwiki_id = my_abandonware_page.get_external_id('PCGamingWiki', r'^https?:\/\/www\.pcgamingwiki\.com\/wiki\/([^\s]+)')
        if not pcgamingwiki_id is None:
            wikidata_game = find_item_from_external_id('P6337', pcgamingwiki_id)
            if wikidata_game is not None:
                print(f"Found matching Wikidata {wikidata_game}")

                claims = wikidata_game.get(u'claims')
                if u'P12652' in claims[u'claims']:
                    print(u'Skipping: Already has a MyAbandonware game ID!')
                else:
                    my_abandonware_claim = pywikibot.Claim(repo, u'P12652')
                    my_abandonware_claim.setTarget(str(my_abandonware_id))
                    wikidata_game.addClaim(my_abandonware_claim, summary=u'based on PCGamingWiki ID.')
                    print(f"Adding MyAbandonware game ID.")

                print()
                continue
            else:
                print(f"No match in Wikidata.")

        wikipedia_slug = my_abandonware_page.get_external_id('Wikipedia Entry', r'^https?:\/\/en\.wikipedia\.org\/wiki\/([^\s]+)')
        if not wikipedia_slug is None:
            wikidata_game = get_wikidata_item_from_wikipedia_slug(wikipedia_slug)
            if wikidata_game is not None:
                print(f"Found matching Wikidata {wikidata_game}")

                claims = wikidata_game.get(u'claims')
                if u'P12652' in claims[u'claims']:
                    print(u'Skipping: Already has a MyAbandonware game ID!')
                else:
                    my_abandonware_claim = pywikibot.Claim(repo, u'P12652')
                    my_abandonware_claim.setTarget(str(my_abandonware_id))
                    wikidata_game.addClaim(my_abandonware_claim, summary=u'based on Wikipedia slug.')
                    print(f"Adding MyAbandonware game ID.")

                print()
                continue
            else:
                print(f"No match in Wikidata.")

        mobygames_id = my_abandonware_page.get_external_id('MobyGames', r'^https?:\/\/(?:www\.)?mobygames\.com\/game\/([1-9]\d*)\/[a-z\d\_\-]+(?:\-[a-z\d\_\-]+)*\/?')
        if not mobygames_id is None:
            wikidata_game = find_item_from_external_id('P11688', mobygames_id)
            if wikidata_game is not None:
                print(f"Found matching Wikidata {wikidata_game}")

                claims = wikidata_game.get(u'claims')
                if u'P12652' in claims[u'claims']:
                    print(u'Skipping: Already has a MyAbandonware game ID!')
                else:
                    my_abandonware_claim = pywikibot.Claim(repo, u'P12652')
                    my_abandonware_claim.setTarget(str(my_abandonware_id))
                    wikidata_game.addClaim(my_abandonware_claim, summary=u'based on MobyGames numeric ID.')
                    print(f"Adding MyAbandonware game ID.")

                print()
                continue
            else:
                print(f"No match in Wikidata.")

        gog_id = my_abandonware_page.get_external_id('Buy on GOG', r'^https?:\/\/(?:af\.)?gog\.com\/(game\/\w+)*\/?')
        if not gog_id is None:
            wikidata_game = find_item_from_external_id('P2725', gog_id)
            if wikidata_game is not None:
                print(f"Found matching Wikidata {wikidata_game}")

                claims = wikidata_game.get(u'claims')
                if u'P12652' in claims[u'claims']:
                    print(u'Skipping: Already has a MyAbandonware game ID!')
                else:
                    my_abandonware_claim = pywikibot.Claim(repo, u'P12652')
                    my_abandonware_claim.setTarget(str(my_abandonware_id))
                    wikidata_game.addClaim(my_abandonware_claim, summary=u'based on GOG ID.')
                    print(f"Adding MyAbandonware game ID.")

                print()
                continue
            else:
                print(f"No match in Wikidata.")

        steam_id = my_abandonware_page.get_external_id('Buy on Steam', r'^https?:\/\/(?:store\.)?steampowered\.com\/app\/(\d+)')
        if not steam_id is None:
            wikidata_game = find_item_from_external_id('P1733', steam_id)
            if wikidata_game is not None:
                print(f"Found matching Wikidata {wikidata_game}")

                claims = wikidata_game.get(u'claims')
                if u'P12652' in claims[u'claims']:
                    print(u'Skipping: Already has a MyAbandonware game ID!')
                else:
                    my_abandonware_claim = pywikibot.Claim(repo, u'P12652')
                    my_abandonware_claim.setTarget(str(my_abandonware_id))
                    wikidata_game.addClaim(my_abandonware_claim, summary=u'based on Steam ID.')
                    print(f"Adding MyAbandonware game ID.")

                print()
                continue
            else:
                print(f"No match in Wikidata.")

        print()
