#!/usr/bin/python3

import urllib.request
import re
import pywikibot
from pywikibot.data.sparql import SparqlQuery

repo = pywikibot.Site()
repo.login()


def query_wikidata():
    sparql = SparqlQuery()
    try:
        result = sparql.select(f"""
            SELECT ?item ?id WHERE {{
            ?item wdt:P6666 ?id.
        }}
    """)
    except Exception as error:
        pywikibot.stdout(error)

    return result


def scrape_repositories(id):
    url = f"https://libregamewiki.org/index.php?title={id}&action=raw"
    print(f"Processing URL {url}")
    try:
        request = urllib.request.Request(url, None)
        response = urllib.request.urlopen(request)
        text = response.read().decode("utf-8")
    except Exception as error:
        pywikibot.stdout(error)
        return None

    # Using regex to extract package IDs for each distribution, ignoring spaces around "="
    matches = re.findall(r'\|(\w+)\s*=\s*([\w\-./]+)', text)
    return dict(matches)


def get_entity(item):
    match = re.match(r"^https?://www\.wikidata\.org/entity/(Q\d+)$", item)
    if not match:
        return None
    return pywikibot.ItemPage(repo, match.group(1))


def add_properties(repos, entity):
    claims = entity.get('claims')

    if "Arch" in repos:
        set_claim(repos["Arch"], claims, 'P3454', "Arch Linux package")
    if "Debian" in repos:
        set_claim(repos["Debian"], claims, 'P3442', "Debian stable package")
    if "Fedora" in repos:
        set_claim(repos["Fedora"], claims, 'P3463', "Fedora package")
    if "Ubuntu" in repos:
        set_claim(repos["Ubuntu"], claims, 'P3473', "Ubuntu package")
    if "Gentoo" in repos:
        set_claim(repos["Gentoo"], claims, 'P3499', "Gentoo package")
    if "Mageia" in repos:
        set_claim(repos["Mageia"], claims, 'P12030', "Mageia package")
    if "Suse" in repos:
        set_claim(repos["Suse"], claims, 'P7788', "openSUSE package")
    if "Flatpak" in repos:
        set_claim(repos["Flatpak"], claims, 'P4655', "Flathub package")
    if "Snap" in repos:
        set_claim(repos["Snap"], claims, 'P4435', "snap package")
    if "FreeBSD" in repos:
        set_claim(repos["FreeBSD"], claims, 'P7427', "FreeBSD port")
    if "Slack" in repos:
        set_claim(f"games/{repos['Slack']}", claims, 'P12077', "SlackBuild package")


def set_claim(package, claims, property, name):
    if property in claims['claims']:
        print(f'Already has {name}. Skipping.')
    else:
        claim = pywikibot.Claim(repo, property)
        claim.setTarget(package)
        entity.addClaim(claim, summary=f'Add {name} from LibreGameWiki.')
        print(f"Adding {name} package.")


if __name__ == '__main__':
    for result in query_wikidata():
        repos = scrape_repositories(result["id"])
        if repos is None:
            continue
        entity = get_entity(result["item"])
        add_properties(repos, entity)
