#!/usr/bin/python3

import pywikibot
import requests
import re
from bs4 import BeautifulSoup
from pywikibot.data.sparql import SparqlQuery
from pywikibot import Claim

site = pywikibot.Site("wikidata", "wikidata")
repo = site.data_repository()


def fetch_product_ids():
    response = requests.get("https://www.gogdb.org/data/products/")
    soup = BeautifulSoup(response.text, 'html.parser')

    rows = soup.find_all('tr')

    product_ids = []
    for row in rows:
        link = row.find('a')
        if link and '/' in link['href']:
            product_id = link['href'].strip('/')
            if product_id.isnumeric():
                product_ids.append(product_id)

    return product_ids


def get_slug(product_id):
    url = f"https://www.gogdb.org/data/products/{str(product_id)}/product.json"
    response = requests.get(url)
    try:
        data = response.json()
        return "game/"+data.get('slug')
    except:
        pywikibot.stdout("Failed to retrieve slug for product id " + product_id)
        return None


def add_qualifier_to_property(item, property_id, qualifier_prop_id, qualifier_value):
    for claim in item.claims[property_id]:
        qualifier = Claim(repo, qualifier_prop_id)
        qualifier.setTarget(str(qualifier_value))

        if qualifier_prop_id not in claim.qualifiers:
            claim.addQualifier(qualifier, summary='based on GOG Database')
            print(f"Adding new qualifier {qualifier_prop_id}.")
        else:
            print(f"Qualifier {qualifier_prop_id} already exists for this claim.")


def find_item_from_external_id(property, id):
    sparql = SparqlQuery()
    try:
        results = sparql.select(f"""
            SELECT ?item WHERE {{
            ?item wdt:{property} "{id}" .
            }}
        """)
    except Exception as error:
        pywikibot.error("Failed to query for {id}", error)
        return None

    if len(results) == 0:
        return None
    match = re.match(r"^https?://www\.wikidata\.org/entity/(Q\d+)$", results[0]["item"])
    if not match:
        return None
    return pywikibot.ItemPage(repo, match.group(1))


product_ids = fetch_product_ids()
for product_id in product_ids:
    print("https://www.gogdb.org/data/product/" + product_id)
    wikidata_game = find_item_from_external_id('P2725', get_slug(product_id))
    if wikidata_game is not None:
        print(f"Found matching Wikidata {wikidata_game}")
        add_qualifier_to_property(wikidata_game, 'P2725', 'P12727', product_id)
    else:
        print(f"No match in Wikidata.")
    print()
