#!/usr/bin/python3

import urllib.request
import re
import pywikibot
from pywikibot.data.sparql import SparqlQuery
from bs4 import BeautifulSoup

repo = pywikibot.Site()
repo.login()

class DatabaseEntry():

    def __init__(self, atarilegend_id):
        url = f"http://www.atarilegend.com/games/games_detail.php?game_id={str(atarilegend_id)}"
        print(f"Processing URL {url}")
        try:
            request = urllib.request.Request(url, None)
            response = urllib.request.urlopen(request)
            html = response.read().decode("utf-8")
        except Exception as error:
            pywikibot.output(error)
            return

        self.html = html

    def get_lemon_amiga_id(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        img_tag = soup.find('img', src=lambda src: src and src.endswith('Amiga.png'))
        if img_tag and img_tag.parent.name == 'a':
            link = img_tag.parent['href']
            match = re.search(r'id=(\d+)', link)
            if match:
                return match.group(1)

    def get_lemon64_id(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        img_tag = soup.find('img', src=lambda src: src and src.endswith('c64.jpg'))
        if img_tag and img_tag.parent.name == 'a':
            link = img_tag.parent['href']
            match = re.search(r'id=(\d+)', link)
            if match:
                return match.group(1)


def find_item_from_external_id(property, id):
    sparql = SparqlQuery()
    try:
        results = sparql.select(f"""
            SELECT ?item WHERE {{
            ?item wdt:{property} "{id}" .
            }}
        """)
    except Exception as error:
        pywikibot.error("Failed to query for {id}", error)
        return None

    if len(results) == 0:
        return None
    match = re.match(r"^https?://www\.wikidata\.org/entity/(Q\d+)$", results[0]["item"])
    if not match:
        return None
    return pywikibot.ItemPage(repo, match.group(1))


if __name__ == "__main__":
    for atarilegend_id in range(1, 7714):
        atarilegend_entry = DatabaseEntry(atarilegend_id)

        amiga_id = atarilegend_entry.get_lemon_amiga_id()
        if not amiga_id is None:
            wikidata_game = find_item_from_external_id('P4846', amiga_id)
            if wikidata_game is not None:
                print(f"Found matching Wikidata {wikidata_game}")

                claims = wikidata_game.get(u'claims')
                if u'P4858' in claims[u'claims']:
                    print(u'Skipping: Already has an Atari Legend ID!')
                    print()
                    continue
                else:
                    atarilegend_claim = pywikibot.Claim(repo, u'P4858')
                    atarilegend_claim.setTarget(str(atarilegend_id))
                    wikidata_game.addClaim(atarilegend_claim, summary=u'based on Lemon Amiga ID.')
                    print(f"Adding Atari Legend ID.")
            else:
                print(f"No match in Wikidata.")

        c64_id = atarilegend_entry.get_lemon64_id()
        if not c64_id is None:
            wikidata_game = find_item_from_external_id('P4816', c64_id)
            if wikidata_game is not None:
                print(f"Found matching Wikidata {wikidata_game}")

                claims = wikidata_game.get(u'claims')
                if u'P4858' in claims[u'claims']:
                    print(u'Skipping: Already has an Atari Legend ID!')
                    print()
                    continue
                else:
                    atarilegend_claim = pywikibot.Claim(repo, u'P4858')
                    atarilegend_claim.setTarget(str(atarilegend_id))
                    wikidata_game.addClaim(atarilegend_claim, summary=u'based on Lemon 64 ID.')
                    print(f"Adding Atari Legend ID.")
            else:
                print(f"No match in Wikidata.")

        print()
