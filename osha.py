#!/usr/bin/python3

from selenium import webdriver
import re
import pywikibot
from pywikibot.data.sparql import SparqlQuery
from bs4 import BeautifulSoup

repo = pywikibot.Site()
repo.login()

class ChemicalData:
    def __init__(self, osha_id):
        self.osha_id = osha_id
        self.html = self.fetch_html(osha_id)

    def fetch_html(self, osha_id):
        url = f"https://www.osha.gov/chemicaldata/{osha_id}"
        print(f"Processing URL {url}")

        options = webdriver.ChromeOptions()
        options.add_argument("window-size=1920x1080")
        options.add_argument(f'user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36')
        options.add_argument('headless')
        driver = webdriver.Chrome(options=options)

        try:
            driver.get(url)
            html = driver.page_source
            driver.quit()
            return html
        except Exception as error:
            pywikibot.output(error)
            driver.quit()
            return None

    def get_cas(self):
        if not self.html:
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        cas_header = soup.find('strong', text=re.compile(r'\s*CAS\s*#\s*'))
        if cas_header:
            cas_content = cas_header.parent.find_next_sibling('td')
            if cas_content:
                cas_number = cas_content.text.strip()
                print(f"Extracted CAS {cas_number}")
                return cas_number
            else:
                print("Error: no CAS data")
                return None
        else:
            print("Error: CAS header not found")
            return None

def find_item_for_cas(cas):
    sparql = SparqlQuery()
    result = sparql.select(f"""
        SELECT ?item WHERE {{
          ?item wdt:P231 "{cas}" .
        }}
    """)
    if len(result) != 1:
        return None
    match = re.match(r"^https?://www\.wikidata\.org/entity/(Q\d+)$", result[0]["item"])
    if not match:
        return None
    return pywikibot.ItemPage(repo, match.group(1))

if __name__ == "__main__":
    for osha_id in range(1, 2000):
        osha_page = ChemicalData(osha_id)
        cas = osha_page.get_cas()
        if not cas is None:
            wikidata_chemical = find_item_for_cas(cas)
            if wikidata_chemical is not None:
                print(f"Found matching Wikidata {wikidata_chemical}")

                claims = wikidata_chemical.get(u'claims')
                if u'P12594' in claims[u'claims']:
                    print(u'Already has a OSHA Occupational Chemical Database ID!')
                else:
                    cas_claim = pywikibot.Claim(repo, u'P12594')
                    cas_claim.setTarget(str(osha_id))
                    wikidata_chemical.addClaim(cas_claim, summary=u'based on CAS registry number.')
                    print(f"Adding OSHA Occupational Chemical Database ID")
            else:
                print(f"No match in Wikidata.")
        print()
