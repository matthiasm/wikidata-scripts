#!/usr/bin/python3

import urllib.request
import re
import pywikibot
from pywikibot.data.sparql import SparqlQuery
from bs4 import BeautifulSoup

repo = pywikibot.Site()
repo.login()

class DatabaseEntry():

    def __init__(self, tuxdb_id):
        url = f"https://tuxdb.com/game/{str(tuxdb_id)}"
        pywikibot.output(f"Processing URL {url}")
        try:
            request = urllib.request.Request(url, None)
            response = urllib.request.urlopen(request)
            html = response.read().decode("utf-8")
        except Exception as error:
            pywikibot.output(error)
            return

        self.html = html


    def get_steam_id(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        td_element = soup.find('td', text="Steam store")
        if td_element:
            a_element = td_element.find_next('a')
            if a_element:
                href = a_element['href']
                numeric_id_match = re.search(r'/(\d+)$', href)
                if numeric_id_match:
                    numeric_id = numeric_id_match.group(1)
                    return numeric_id
                else:
                   print("Failed to extract Steam application ID.")
            else:
                print("No Steam application ID found.")
        else:
            print("No 'Steam store' entry found.")


def find_item_from_steam_id(application_id):
    sparql = SparqlQuery()
    result = sparql.select(f"""
        SELECT ?item WHERE {{
          ?item wdt:P1733 "{application_id}" .
        }}
    """)
    if len(result) != 1:
        return None
    match = re.match(r"^https?://www\.wikidata\.org/entity/(Q\d+)$", result[0]["item"])
    if not match:
        return None
    return pywikibot.ItemPage(repo, match.group(1))


if __name__ == "__main__":
    for tuxdb_id in reversed(range(1, 11407)):
        tuxdb_page = DatabaseEntry(tuxdb_id)
        application_id = tuxdb_page.get_steam_id()
        if not application_id is None:
            wikidata_game = find_item_from_steam_id(application_id)
            if wikidata_game is not None:
                print(f"Found matching Wikidata {wikidata_game}")

                claims = wikidata_game.get(u'claims')
                if u'P11307' in claims[u'claims']:
                    print(u'Skipping: Already has a TuxDB ID!')
                else:
                    tuxdb_claim = pywikibot.Claim(repo, u'P11307')
                    tuxdb_claim.setTarget(str(tuxdb_id))
                    wikidata_game.addClaim(tuxdb_claim, summary=u'Add TuxDB ID based on Steam application ID.')
                    print(f"Adding TuxDB ID.")
            else:
                print(f"No match in Wikidata.")
        print()
