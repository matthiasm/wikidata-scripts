#!/usr/bin/python3

import re
import pywikibot
import urllib.request
from pywikibot.data.sparql import SparqlQuery
from bs4 import BeautifulSoup

site = pywikibot.Site(url='https://www.c64-wiki.de', fam='c64')

repo = pywikibot.Site()
repo.login()


def find_item_from_external_id(property, id):
    sparql = SparqlQuery()
    try:
        results = sparql.select(f"""
            SELECT ?item WHERE {{
            ?item wdt:{property} "{str(id)}" .
            }}
        """)
    except Exception as error:
        pywikibot.error("Failed to query for {id}", error)
        return None

    if results is None:
        return None

    if len(results) == 0:
        return None
    match = re.match(r"^https?://www\.wikidata\.org/entity/(Q\d+)$", results[0]["item"])
    if not match:
        return None
    return pywikibot.ItemPage(repo, match.group(1))


def extract_template_id(source_content):
    match = re.search(r"{{gamebase\|(\d+)}}", source_content)
    if match:
        return match.group(1)
    else:
        return None


def claim(wikidata_game, property, id, name, base):
    claims = wikidata_game.get(u'claims')
    if property in claims[u'claims']:
        print(f'Skipping: Already has a {name} ID!')
    else:
        claim = pywikibot.Claim(repo, property)
        claim.setTarget(str(id))
        wikidata_game.addClaim(claim, summary=f'based on {base} ID.')
        print(f"Adding {name} ID.")


def harvest_c64wiki():
    category = pywikibot.Category(site, 'Kategorie:Spiel')
    for page in list(category.articles()):
        if page.title() == "C64-Wiki:Musterartikel (Spiel)":
            continue

        if page.title() == "Testartikel (Spielvoting)":
            continue

        if "Übungsartikel" in page.title():
            continue

        print(page.title())
        gb64_id = extract_template_id(page.text)
        if gb64_id is None:
            continue

        wikidata_game = find_item_from_external_id('P11129', str(page.title().replace(' ', '_')))
        if wikidata_game is not None:
            print(f"Found matching Wikidata {wikidata_game}")
            claim(wikidata_game, u'P4917', gb64_id, 'Gamebase 64', 'C64-Wiki')
        else:
            print(f"No match in Wikidata.")

        print()


headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3'
}

class Lemon64Entry():

    def __init__(self, lemon_id):
        url = f"https://www.lemon64.com/?game_id={str(lemon_id)}"
        print(f"Processing URL {url}")
        try:
            request = urllib.request.Request(url, headers=headers)
            response = urllib.request.urlopen(request)
            html = response.read().decode("utf-8")
        except Exception as error:
            pywikibot.output(error)
            return

        self.html = html

    def get_gamebase64_id(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        a = soup.find('a', title ='External link: More information on Gamebase64')
        if a and 'href' in a.attrs:
            match = re.match(r"^https?://gb64\.com/game\.php\?id=(\d+)$", a['href'])
            if match:
                return match.group(1)
        else:
            return None


def harvest_lemon64():
    for lemon_id in range(1, 6873):
        lemon_page = Lemon64Entry(lemon_id)
        gb64_id = lemon_page.get_gamebase64_id()
        if gb64_id is None:
            continue

        wikidata_game = find_item_from_external_id('P4816', lemon_id)
        if wikidata_game is not None:
            print(f"Found matching Wikidata {wikidata_game}")
            claim(wikidata_game, u'P4917', gb64_id, 'Gamebase 64', 'Lemon64')
        else:
            print(f"No match in Wikidata.")

        print()


#harvest_c64wiki()
harvest_lemon64()

