#!/usr/bin/python3

import urllib.request
import re
import pywikibot
from pywikibot.data.sparql import SparqlQuery
from bs4 import BeautifulSoup

repo = pywikibot.Site()
repo.login()

replacements = [
    (r"&nbsp;", ""),
]

class CameoChemicalsPage():

    def __init__(self, cameo_id):
        url = f"https://cameochemicals.noaa.gov/chemical/{str(cameo_id)}"
        print(f"Processing URL {url}")
        try:
            request = urllib.request.Request(url, None)
            response = urllib.request.urlopen(request)
            html = response.read().decode("utf-8")
        except Exception as error:
            pywikibot.stdout(error)
            return

        self.cameo_id = cameo_id
        self.html = html

    def get_cas(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'lxml')
        td_with_th_cas = soup.find('td', headers='th-cas')
        li_content = td_with_th_cas.find('li')
        if li_content is None:
            print(f"No CAS data")
            return None

        text_content = li_content.get_text(strip=True)
        cas = text_content
        print(f"Extracted CAS {cas}")
        for (matcher, replacer) in replacements:
            cas = re.sub(matcher, replacer, cas)
        return cas.strip()

def find_item_for_cas(cas):
    sparql = SparqlQuery()
    result = sparql.select(f"""
        SELECT ?item WHERE {{
          ?item wdt:P231 "{cas}" .
        }}
    """)
    if len(result) != 1:
        return None
    match = re.match(r"^https?://www\.wikidata\.org/entity/(Q\d+)$", result[0]["item"])
    if not match:
        return None
    return pywikibot.ItemPage(repo, match.group(1))

if __name__ == "__main__":
    for cameo_id in range(30000, 30100):
        cameo_page = CameoChemicalsPage(cameo_id)
        cas = cameo_page.get_cas()
        if not cas is None:
            wikidata_chemical = find_item_for_cas(cas)
            if wikidata_chemical is not None:
                print(f"Found matching Wikidata {wikidata_chemical}")

                claims = wikidata_chemical.get(u'claims')
                if u'P11931' in claims[u'claims']:
                    print(u'Already has a CAMEO Chemicals ID!')
                else:
                    cas_claim = pywikibot.Claim(repo, u'P11931')
                    cas_claim.setTarget(str(cameo_id))
                    wikidata_chemical.addClaim(cas_claim, summary=u'Add CAMEO Chemicals ID based on CAS registry number.')
                    print(f"Adding CAMEO Chemicals ID")
            else:
                print(f"No match in Wikidata.")
