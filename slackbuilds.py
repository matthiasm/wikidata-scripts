#!/usr/bin/python3

import pywikibot
import urllib.request
from bs4 import BeautifulSoup
import re
from pywikibot.data.sparql import SparqlQuery

repo = pywikibot.Site()
repo.login()

def get_packages(category):
    url = f"https://slackbuilds.org/repository/15.0/{str(category)}/"
    print(f"Processing URL {url}")
    try:
        request = urllib.request.Request(url, None)
        response = urllib.request.urlopen(request)
        html = response.read().decode("utf-8")
    except Exception as error:
        pywikibot.stdout(error)

    soup = BeautifulSoup(html, 'lxml')
    for a in soup.find('table', {'id': 'roll_table'}).find_all('a'):
        yield a.get_text()


def get_website_url(category, package):
    url = f"https://slackbuilds.org/repository/15.0/{category}/{package}/"
    pywikibot.output(f"Processing URL {url}")
    try:
        request = urllib.request.Request(url, None)
        response = urllib.request.urlopen(request)
        html_reponse = response.read().decode("utf-8")
    except Exception as error:
        pywikibot.stdout(error)
        return None

    try:
        soup = BeautifulSoup(html_reponse, 'lxml')
        homepage_label = soup.find('b', text="Homepage:")
        if homepage_label:
            homepage_url = homepage_label.find_next('a', href=True)
            if homepage_url:
                return homepage_url.get('href')
            else:
                print("Homepage URL not found.")
                return None
    except Exception as error:
        pywikibot.stdout(error)
        return None


def find_item_from_official_website(url):
    variations = []
    variations.append(url if url.endswith('/') else url + '/')
    variations.append(url[:-1] if url.endswith('/') else url)

    for variation in variations:
        print(f"Searching website {variation}")
        sparql = SparqlQuery()
        try:
            result = sparql.select(f"""
                SELECT ?item WHERE {{
                    ?item wdt:P856 <{variation}> .
                }}
            """)
        except Exception as error:
            pywikibot.output(error)
            continue
        if result is None:
            continue
        if len(result) != 1:
            continue
        match = re.match(r"^https?://www\.wikidata\.org/entity/(Q\d+)$", result[0]["item"])
        if not match:
            return None
        return pywikibot.ItemPage(repo, match.group(1))
    return None


def extract_sourceforge_project(url):
    if url is None:
        return

    match = re.search(r'sourceforge\.net/p/([^/]+)', url)
    if match:
        return match.group(1)

    match = re.search(r'http://([^\.]+)\.sourceforge\.net', url)
    if match:
        return match.group(1)

    match = re.search(r'sourceforge\.net/projects/([^/]+)', url)
    if match:
        return match.group(1)


def find_item_from_sourceforge_project(item):
    if item is None:
        return None

    print(f"Searching SourceForge {item}")
    sparql = SparqlQuery()
    try:
        result = sparql.select(f"""
            SELECT ?item WHERE {{
                ?item wdt:P2209 <{item}> .
            }}
        """)
    except Exception as error:
        pywikibot.output(error)
        return None
    if result is None:
        return None
    if len(result) != 1:
        return None
    match = re.match(r"^https?://www\.wikidata\.org/entity/(Q\d+)$", result[0]["item"])
    if not match:
        return None
    return pywikibot.ItemPage(repo, match.group(1))


def extract_launchpad_id(url):
    if url is None:
        return None

    match = re.search(r'launchpad\.net/([^/]+)', url)
    if match:
        return match.group(1)
    return None


def find_item_from_launchpad_project(item):
    print(f"Searching Launchpad {item}")
    sparql = SparqlQuery()
    try:
        result = sparql.select(f"""
            SELECT ?item WHERE {{
                ?item wdt:P3802 <{item}> .
            }}
        """)
    except Exception as error:
        pywikibot.output(error)
        return None
    if result is None:
        return None
    if len(result) != 1:
        return None
    match = re.match(r"^https?://www\.wikidata\.org/entity/(Q\d+)$", result[0]["item"])
    if not match:
        return None
    return pywikibot.ItemPage(repo, match.group(1))


def find_item_from_source_code_repo(url):
    print(f"Searching Repository {url}")
    sparql = SparqlQuery()
    try:
        result = sparql.select(f"""
            SELECT ?item WHERE {{
                ?item wdt:P1324 <{url}> .
            }}
        """)
    except Exception as error:
        pywikibot.output(error)
        return None
    if result is None:
        return None
    if len(result) != 1:
        return None
    match = re.match(r"^https?://www\.wikidata\.org/entity/(Q\d+)$", result[0]["item"])
    if not match:
        return None
    return pywikibot.ItemPage(repo, match.group(1))


def find_item_from_user_manual(url):
    print(f"Searching user manual URL {url}")
    sparql = SparqlQuery()
    try:
        result = sparql.select(f"""
            SELECT ?item WHERE {{
                ?item wdt:P2078 <{url}> .
            }}
        """)
    except Exception as error:
        pywikibot.output(error)
        return None
    if result is None:
        return None
    if len(result) != 1:
        return None
    match = re.match(r"^https?://www\.wikidata\.org/entity/(Q\d+)$", result[0]["item"])
    if not match:
        return None
    return pywikibot.ItemPage(repo, match.group(1))


categories = [ "academic", "business", "games", "ham", "misc", "office", "ruby", "accessibility", "desktop", "gis", "haskell", "multimedia", "perl", "system", "audio", "development", "graphics", "libraries", "network", "python" ]
for category in categories:
    for package in get_packages(category):
        url = get_website_url(category, package)
        entity = find_item_from_source_code_repo(url)
        if entity is None:
            entity = find_item_from_sourceforge_project(extract_sourceforge_project(url))
        if entity is None:
            entity = find_item_from_launchpad_project(extract_launchpad_id(url))
        if entity is None:
            entity = find_item_from_source_code_repo(url)
        if entity is None:
            entity = find_item_from_official_website(url)
        if entity is None:
            entity = find_item_from_user_manual(url)
        if entity is not None:
            print(f"Found matching Wikidata {entity}")
            claims = entity.get(u'claims')
            if u'P12077' in claims[u'claims']:
                print(u'Skipping: Already has a SlackBuilds package!')
            else:
                claim = pywikibot.Claim(repo, u'P12077')
                claim.setTarget(f"{category}/{package}")
                entity.addClaim(claim, summary=u'Add SlackBuilds package based on URL.')
                print(f"Adding SlackBuilds package")
        print()
