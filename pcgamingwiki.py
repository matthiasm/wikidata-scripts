#!/usr/bin/python3

import urllib.request
import re
import pywikibot
import string
import itertools
from pywikibot.data.sparql import SparqlQuery
from bs4 import BeautifulSoup
import requests

site = pywikibot.Site(url='https://www.pcgamingwiki.com', fam='pcgaming')

repo = pywikibot.Site()
repo.login()

class MyAbandonwareEntry():

    def __init__(self, my_abandonware_id):
        url = f"https://www.myabandonware.com/game/--{str(my_abandonware_id)}"
        print(f"Processing URL {url}")
        try:
            request = urllib.request.Request(url, None)
            response = urllib.request.urlopen(request)
            html = response.read().decode("utf-8")
        except Exception as error:
            pywikibot.output(error)
            return

        self.html = html


    def get_external_id(self, label, pattern):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        a = soup.find('a', string=label)
        if a is None:
            return None
        match = re.match(pattern, a['href'])
        if match:
            return match.group(1)
        else:
            return None


def generate_patterns(start=None):
    characters = string.ascii_lowercase + string.digits
    all_patterns = []
    found_start = start is None

    for length in range(1, 4):
        for combo in itertools.product(characters, repeat=length):
            pattern = ''.join(combo)
            if not found_start:
                if pattern == start:
                    found_start = True
                continue
            all_patterns.append(pattern)

    return all_patterns


def find_item_from_external_id(property, id):
    sparql = SparqlQuery()
    try:
        results = sparql.select(f"""
            SELECT ?item WHERE {{
            ?item wdt:{property} "{id}" .
            }}
        """)
    except Exception as error:
        pywikibot.error("Failed to query for {id}", error)
        return None

    if len(results) == 0:
        return None
    match = re.match(r"^https?://www\.wikidata\.org/entity/(Q\d+)$", results[0]["item"])
    if not match:
        return None
    return pywikibot.ItemPage(repo, match.group(1))


def harvest_myabandonware():
    for my_abandonware_id in generate_patterns():
        my_abandonware_page = MyAbandonwareEntry(my_abandonware_id)

        pcgamingwiki_id = my_abandonware_page.get_external_id('PCGamingWiki', r'^https?:\/\/www\.pcgamingwiki\.com\/wiki\/([^\s]+)')
        if not pcgamingwiki_id is None:
            pcgamingwiki_id = pcgamingwiki_id.replace('%26', '&').replace('%27', '\'')
            wikidata_game = find_item_from_external_id('P12652', my_abandonware_id)
            if wikidata_game is not None:
                print(f"Found matching Wikidata {wikidata_game}")

                claims = wikidata_game.get(u'claims')
                if u'P6337' in claims[u'claims']:
                    print(u'Skipping: Already has a PCGamingWiki ID!')
                else:
                    pcgh_claim = pywikibot.Claim(repo, u'P6337')
                    pcgh_claim.setTarget(str(pcgamingwiki_id))
                    wikidata_game.addClaim(pcgh_claim, summary=u'based on MyAbandonware ID.')
                    print(f"Adding PCGamingWiki ID.")

                print()
                continue
            else:
                print(f"Error: no match in Wikidata.")


def extract_template_parameter(page, id):
    match = re.search(rf"\|{id}\s+\=\s+([\w\s\-\(\)]+)\n", page.text)
    if match:
        return match.group(1).replace(" ", "_")
    else:
        return None


def database_claim(page, property, template_id, identifier_name):
    wikidata_game = find_item_from_external_id(property, extract_template_parameter(page, template_id))
    if wikidata_game is not None:
        print(f"Found matching Wikidata {wikidata_game}")

        claims = wikidata_game.get(u'claims')
        if u'P6337' in claims[u'claims']:
            print(u'Skipping: Already has a PCGamingWiki ID!')
        else:
            pcghwiki_claim = pywikibot.Claim(repo, u'P6337')
            title = str(page.title().replace(' ', '_')).replace('%26', '&').replace('%27', '\'')
            pcghwiki_claim.setTarget(title)
            wikidata_game.addClaim(pcghwiki_claim, summary=u'based on ' + identifier_name)
            print(f"Adding PCGamingWiki ID.")

        print()
        return wikidata_game

    return None


def get_wikidata_item_from_wikipedia_slug(title):
    url = f"https://en.wikipedia.org/w/api.php"
    params = {
        'action': 'query',
        'prop': 'pageprops',
        'titles': title,
        'format': 'json'
    }

    response = requests.get(url, params=params)
    data = response.json()
    pages = data.get('query', {}).get('pages', {})
    if pages:
        for page_id in pages:
            page = pages[page_id]
            wikibase_item = page.get('pageprops', {}).get('wikibase_item')
            if wikibase_item is not None:
                return pywikibot.ItemPage(repo, wikibase_item)

    return None


def english_wikipedia_claim(page):
    title = extract_template_parameter(page, "wikipedia")
    if title is None:
        return

    wikidata_game = get_wikidata_item_from_wikipedia_slug(title)
    if wikidata_game is not None:
        print(f"Found matching Wikidata {wikidata_game}")

        claims = wikidata_game.get(u'claims')
        if u'P6337' in claims[u'claims']:
            print(u'Skipping: Already has a PCGamingWiki ID!')
        else:
            c64wiki_claim = pywikibot.Claim(repo, u'P6337')
            c64wiki_claim.setTarget(str(page.title().replace(' ', '_')))
            wikidata_game.addClaim(c64wiki_claim, summary=u'based on English Wikipedia title')
            print(f"Adding PCGamingWiki ID.")

        print()
        return wikidata_game

    return None


def match_infobox(start=None):
    found_start = start is None
    category = pywikibot.Category(site, 'Category:Games')
    for page in list(category.articles()):
        print(page.title())

        if not found_start:
            if page.title() == start:
                found_start = True
            continue

        wikidata_game = database_claim(page, 'P1733', 'steam appid' ,'Steam application ID')
        if wikidata_game is not None:
            continue

        wikidata_game = database_claim(page, 'P2725', 'gogcom id' ,'GOG application ID')
        if wikidata_game is not None:
            continue

        wikidata_game = database_claim(page, 'P600', 'winehq' ,'Wine AppDB ID')
        if wikidata_game is not None:
            continue

        wikidata_game = database_claim(page, 'P2816', 'hltb' ,'HowLongToBeat ID')
        if wikidata_game is not None:
            continue

        wikidata_game = database_claim(page, 'P5794', 'igdb' ,'Internet Game Database game ID')
        if wikidata_game is not None:
            continue

        wikidata_game = database_claim(page, 'P7597', 'lutris' ,'Lutris game ID')
        if wikidata_game is not None:
            continue

        wikidata_game = database_claim(page, 'P11688', 'mobygames' ,'MobyGames game ID')
        if wikidata_game is not None:
            continue

        wikidata_game = database_claim(page, 'P9075', 'strategywiki' ,'StrategyWiki ID')
        if wikidata_game is not None:
            continue

        wikidata_game = english_wikipedia_claim(page)
        if wikidata_game is not None:
            continue

        wikidata_game = database_claim(page, 'P856', 'official site' ,'official website')
        if wikidata_game is not None:
            continue

        print()


#harvest_myabandonware()
match_infobox()
