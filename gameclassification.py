#!/usr/bin/python3

import urllib.request
import re
import pywikibot
import requests
from pywikibot.data.sparql import SparqlQuery
from bs4 import BeautifulSoup

repo = pywikibot.Site()
repo.login()

class GameClassificationEntry():

    def __init__(self, gc_id):
        url = f"https://gameclassification.com/EN/games/{str(gc_id)}-_/index.html"
        pywikibot.output(f"Processing URL {url}")
        try:
            request = urllib.request.Request(url, None)
            response = urllib.request.urlopen(request)
            self.html = response.read().decode("utf-8")
        except Exception as error:
            pywikibot.output(error)
            return

    def get_wikipedia_article(self, lang):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        header = soup.find('h4', string="Links")
        if header:
            bulletin = header.find_next_sibling('ul', class_='links')
            if bulletin:
                links = bulletin.find_all('a')
                for link in links:
                    if f'{str(lang)}.wikipedia.org/wiki/' in link['href']:
                        return link['href'].split('/')[-1]

    def get_mobygames_slug(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        header = soup.find('h4', string="Links")
        if header:
            bulletin = header.find_next_sibling('ul', class_='links')
            if bulletin:
                links = bulletin.find_all('a')
                for link in links:
                    match = re.search(r'https?:\/\/www\.mobygames\.com\/game\/([a-z0-9_-]+|)', link['href'])
                    if match:
                        return match.group(1)

    def get_gamefaqs_id(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        header = soup.find('h4', string="Links")
        if header:
            bulletin = header.find_next_sibling('ul', class_='links')
            if bulletin:
                links = bulletin.find_all('a')
                for link in links:
                    match = re.search(r'https?:\/\/www\.gamefaqs\.com\/.*\/(\d+).html', link['href'])
                    if match:
                        return match.group(1)


def get_wikidata_item_from_wikipedia_slug(title, lang):
    url = f"https://{str(lang)}.wikipedia.org/w/api.php"
    params = {
        'action': 'query',
        'prop': 'pageprops',
        'titles': title,
        'format': 'json',
        'redirects': 1
    }

    try:
        response = requests.get(url, params=params)
        data = response.json()
        pages = data.get('query', {}).get('pages', {})
        if pages:
            for page_id in pages:
                page = pages[page_id]
                wikibase_item = page.get('pageprops', {}).get('wikibase_item')
                if wikibase_item is not None:
                    return pywikibot.ItemPage(repo, wikibase_item)
    except Exception as error:
            pywikibot.output(error)


def find_item_from_external_id(property, id):
    sparql = SparqlQuery()
    try:
        results = sparql.select(f"""
            SELECT ?item WHERE {{
            ?item wdt:{property} "{id}" .
            }}
        """)
    except Exception as error:
        pywikibot.error("Failed to query for {id}", error)
        return None

    if len(results) == 0:
        return None
    match = re.match(r"^https?://www\.wikidata\.org/entity/(Q\d+)$", results[0]["item"])
    if not match:
        return None
    return pywikibot.ItemPage(repo, match.group(1))


def wikidata_claim(gc_id, wikidata_game, identifier_name):

    print(f"Found matching Wikidata {wikidata_game}")

    if wikidata_game.claims:
        if u'P12215' in wikidata_game.claims:
            print(f"Already has a Game Classification game ID {gc_id}.")
            print()
            return

    gc_claim = pywikibot.Claim(repo, u'P12215')
    gc_claim.setTarget(str(gc_id))
    wikidata_game.addClaim(gc_claim, summary=u'based on ' + identifier_name)
    print(f"Adding Game Classification game ID")
    print()


if __name__ == "__main__":
    for gc_id in range(1, 43525):
        gc_entry = GameClassificationEntry(gc_id)

        wikidata_game = find_item_from_external_id('P4769', gc_entry.get_gamefaqs_id())
        if wikidata_game is not None:
            wikidata_claim(gc_id, wikidata_game, 'GameFAQs ID')
            continue

        wikidata_game = find_item_from_external_id('P1933', gc_entry.get_mobygames_slug())
        if wikidata_game is not None:
            wikidata_claim(gc_id, wikidata_game, 'MobyGames ID')
            continue

        wikidata_game = get_wikidata_item_from_wikipedia_slug(gc_entry.get_wikipedia_article('en'), 'en')
        if wikidata_game is not None:
            wikidata_claim(gc_id, wikidata_game, 'English Wikipedia article')
            continue

        wikidata_game = get_wikidata_item_from_wikipedia_slug(gc_entry.get_wikipedia_article('fr'), 'fr')
        if wikidata_game is not None:
            wikidata_claim(gc_id, wikidata_game, 'French Wikipedia article')
            continue

        print()
