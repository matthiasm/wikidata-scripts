#!/usr/bin/python3

import re
import pywikibot
from pywikibot.data.sparql import SparqlQuery
from pywikibot import ItemPage
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.chrome.options import Options

repo = pywikibot.Site()
repo.login()

chrome_options = Options()
chrome_options.add_argument("--headless")

s = Service('/usr/lib/chromium-browser/chromedriver')

driver = webdriver.Chrome(service=s, options=chrome_options)

class UVListEntry():

    def __init__(self, uvlist_id):
        self.uvlist_id = uvlist_id
        self.html = self.fetch_html(uvlist_id)

    def fetch_html(self, uvlist_id):
        url = f"https://www.uvlist.net/game-{uvlist_id}"
        print(f"Processing URL {url}")

        try:
            driver.get(url)
            html = driver.page_source
            return html
        except Exception as error:
            pywikibot.output(error)
            return None

    def get_lemonamiga_id(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        a = soup.find('a', href=lambda x: x and 'lemonamiga.com' in x)
        if a and 'href' in a.attrs:
            match = re.match(r"^https?://www\.lemonamiga\.com/games/details\.php\?id=(\d+)$", a['href'])
            if match:
                return match.group(1)

    def get_dosbox_compatibility_id(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        a = soup.find('a', href=lambda x: x and 'dosbox.com' in x)
        if a and 'href' in a.attrs:
            match = re.match(r"^https?://www\.dosbox\.com/comp_list\.php\?showID=(\d+)$", a['href'])
            if match:
                return match.group(1)

    def get_steam_application_id(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        a = soup.find('a', href=lambda x: x and 'steampowered.com' in x)
        if a and 'href' in a.attrs:
            match = re.match(r"^https?://store\.steampowered\.com/app/(\d+)/$", a['href'])
            if match:
                return match.group(1)

    def get_gog_application_id(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        a = soup.find('a', href=lambda x: x and 'gog.com' in x)
        if a and 'href' in a.attrs:
            match = re.match(r"^https?://www\.gog\.com/game/(\w+)$", a['href'])
            if match:
                return match.group(1)

    def get_epic_store_id(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        a = soup.find('a', href=lambda x: x and 'epicgames.com' in x)
        if a and 'href' in a.attrs:
            match = re.match(r"^https?://www\.epicgames\.com/store/en-US/product/(\S+)/home$", a['href'])
            if match:
                return match.group(1)

    def get_metacritic_game_id(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        a = soup.find('a', href=lambda x: x and 'metacritic.com' in x, string="metacritic.com")
        if a and 'href' in a.attrs:
            match = re.match(r"^https?://www\.metacritic\.com/game/(\S+)/$", a['href'])
            if match:
                return match.group(1)

    def get_gamefaqs_id(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        a = soup.find('a', href=lambda x: x and 'gamefaqs.gamespot.com' in x)
        if a and 'href' in a.attrs:
            match = re.match(r"^https?://gamefaqs\.gamespot\.com/p/(\d+)-/data$", a['href'])
            if match:
                return match.group(1)

    def get_howlongtobeat_id(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        a = soup.find('a', href=lambda x: x and 'howlongtobeat.com' in x)
        if a and 'href' in a.attrs:
            match = re.match(r"^https?://www\.howlongtobeat\.com/game\.php\?id=(\d+)$", a['href'])
            if match:
                return match.group(1)

    def get_itch_url(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        xrefs = soup.find('h2', {'id': 'acc_xrefs'})
        if xrefs:
            return next((a['href'] for a in xrefs.parent.find_all('a', href=True) if 'itch.io' in a['href']), None)

    def get_indiedb_id(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        a = soup.find('a', href=lambda x: x and 'indiedb.com' in x)
        if a and 'href' in a.attrs:
            match = re.match(r"^https?://www\.indiedb\.com/games/(\S+)$", a['href'])
            if match:
                return match.group(1)


def find_item_from_external_id(property, id) -> ItemPage:
    sparql = SparqlQuery()
    try:
        results = sparql.select(f"""
            SELECT ?item WHERE {{
            ?item wdt:{property} "{str(id)}" .
            }}
        """)
    except Exception as error:
        pywikibot.error("Failed to query for {id}", error)
        return None

    if results is None:
        return None

    if len(results) == 0:
        return None

    match = re.match(r"^https?://www\.wikidata\.org/entity/(Q\d+)$", results[0]["item"])
    if not match:
        return None

    return ItemPage(repo, match.group(1))


def database_claim(uvlist_id, external_property, external_id, identifier_name):
    wikidata_game = find_item_from_external_id(external_property, external_id)
    if wikidata_game is not None:
        print(f"Found matching Wikidata {wikidata_game}")

        if wikidata_game.claims:
            if u'P7555' in wikidata_game.claims:
                for claim in wikidata_game.claims['P7555']:
                    if claim.getTarget() == str(uvlist_id):
                        print(f"Already has UVList game ID {uvlist_id}.")
                        return wikidata_game

        uvlist_claim = pywikibot.Claim(repo, u'P7555')
        uvlist_claim.setTarget(str(uvlist_id))
        wikidata_game.addClaim(uvlist_claim, summary=u'based on ' + identifier_name)
        print(f"Adding UVList game ID")

        print()
        return wikidata_game

    return None


if __name__ == "__main__":
    for uvlist_id in range(1, 158231):
        uvlist_entry = UVListEntry(uvlist_id)

        wikidata_game = database_claim(uvlist_id, 'P6278', uvlist_entry.get_epic_store_id(), 'Epic Games Store ID')
        if wikidata_game is not None:
            continue

        wikidata_game = database_claim(uvlist_id, 'P11572', uvlist_entry.get_dosbox_compatibility_id(), 'DOSBox Compatibility ID')
        if wikidata_game is not None:
            continue

        wikidata_game = database_claim(uvlist_id, 'P4769', uvlist_entry.get_gamefaqs_id(), 'GameFAQs game ID')
        if wikidata_game is not None:
            continue

        wikidata_game = database_claim(uvlist_id, 'P2725', uvlist_entry.get_gog_application_id(), 'GOG application ID')
        if wikidata_game is not None:
            continue

        wikidata_game = database_claim(uvlist_id, 'P2816', uvlist_entry.get_howlongtobeat_id(), 'HowLongToBeat ID')
        if wikidata_game is not None:
            continue

        wikidata_game = database_claim(uvlist_id, 'P7294', uvlist_entry.get_itch_url(), 'itch.io URL')
        if wikidata_game is not None:
            continue

        wikidata_game = database_claim(uvlist_id, 'P6717', uvlist_entry.get_indiedb_id(), 'Indie DB game ID')
        if wikidata_game is not None:
            continue

        wikidata_game = database_claim(uvlist_id, 'P4846', uvlist_entry.get_lemonamiga_id(), 'Lemon Amiga ID')
        if wikidata_game is not None:
            continue

        wikidata_game = database_claim(uvlist_id, 'P12054', uvlist_entry.get_metacritic_game_id(), 'Metacritic game ID')
        if wikidata_game is not None:
            continue

        wikidata_game = database_claim(uvlist_id, 'P1733', uvlist_entry.get_steam_application_id(), 'Steam application ID')
        if wikidata_game is not None:
           continue

    driver.quit()
