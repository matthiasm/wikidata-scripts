#!/usr/bin/python3

import pywikibot
import urllib.request
from bs4 import BeautifulSoup
import re
from pywikibot.data.sparql import SparqlQuery

from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

repo = pywikibot.Site()
repo.login()

chrome_options = Options()
chrome_options.add_argument("--headless")

s = Service('/usr/bin/chromedriver')

driver = webdriver.Chrome(service=s, options=chrome_options)

def get_packages():
    print("Getting package list from SUSE Package Hub...")
    driver.get('https://packagehub.suse.com/')
    WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'packagelist')))
    div_element = driver.find_element(By.ID, 'packagelist')
    links = div_element.find_elements(By.TAG_NAME, 'a')
    return [link.text for link in links]


def get_website_url(package):
    url = f"https://packagehub.suse.com/packages/{package}"
    print(f"Processing URL {url}")
    try:
        request = urllib.request.Request(url, None)
        response = urllib.request.urlopen(request)
        html_reponse = response.read().decode("utf-8")
    except Exception as error:
        pywikibot.stdout(error)
        return None

    try:
        soup = BeautifulSoup(html_reponse, 'lxml')
        return soup.find('b', string='URL: ').find_next_sibling('a')['href']
    except Exception as error:
        pywikibot.stdout(error)
        return None


def extract_sourceforge_project(url):
    if url is None:
        return

    match = re.search(r'https?://sourceforge\.net/p(?:rojects)?/([\w.-]+)/?', url)
    if match:
        return match.group(1)

    match = re.search(r'http://([^\.]+)\.sourceforge\.net', url)
    if match:
        return match.group(1)

def extract_launchpad_id(url):
    if url is None:
        return None

    match = re.search(r'launchpad\.net/([^/]+)', url)
    if match:
        return match.group(1)
    return None


def extract_savannah_project(url):
    if url is None:
        return

    match = re.search(r'savannah\.gnu\.org/projects/([^/]+)', url)
    if match:
        return match.group(1)


def generate_url_variations(url):
    from urllib.parse import urlparse, urlunparse

    parsed_url = urlparse(url)
    schemes = ['http', 'https']
    www_variants = ['', 'www.']
    slash_variants = ['', '/']

    variations = set()
    for scheme in schemes:
        for www in www_variants:
            for slash in slash_variants:
                netloc = parsed_url.netloc

                if netloc == 'www.':
                    return None
                if netloc == 'sourceforge.net':
                    return None
                if netloc == 'www.tinkerforge.com':
                    return None
                if netloc == 'www.kde.org':
                    return None
                if netloc == 'kernel.org' or netloc == 'www.kernel.org':
                    return None
                if netloc == 'www.gnome.org':
                    return None
                if netloc == 'elementary.io':
                    return None
                if netloc == 'www.debian.org':
                    return None
                if netloc == 'www.opensuse.org':
                    return None
                if netloc == 'www.suse.com' or netloc == 'suse.com':
                    return None
                if netloc == 'www.debian.org':
                    return None

                if www and not netloc.startswith('www.'):
                    netloc = www + netloc
                elif not www and netloc.startswith('www.'):
                    netloc = netloc[4:]  # remove 'www.'

                modified_url = parsed_url._replace(scheme=scheme, netloc=netloc)
                complete_url = urlunparse(modified_url)
                if slash and not complete_url.endswith('/'):
                    complete_url += '/'
                elif not slash and complete_url.endswith('/'):
                    complete_url = complete_url.rstrip('/')
                variations.add(complete_url)

    return list(variations)


def find_item_from_external_id(property, id):
    sparql = SparqlQuery()
    try:
        results = sparql.select(f"""
            SELECT ?item WHERE {{
            ?item wdt:{property} {id} .
            }}
        """)
    except Exception as error:
        pywikibot.error("Failed to query for {id}", error)
        return None

    if results is None:
        return None

    if len(results) == 0:
        return None
    match = re.match(r"^https?://www\.wikidata\.org/entity/(Q\d+)$", results[0]["item"])
    if not match:
        return None
    return pywikibot.ItemPage(repo, match.group(1))


start_processing = False
for package in get_packages():
    if not start_processing:
        if package == 'rubygem-hpricot':
            start_processing = True
        else:
            continue

    if package.startswith('mingw64-') or package.startswith('mingw32-'):
        continue

    if package.startswith('python2-') or package.startswith('python3-'):
        continue

    print(f"openSUSE package {package}")

    base = None
    entity = None

    if package.startswith('ghc-'):
        entity = find_item_from_external_id('P11246', f"\"{package.split('ghc-')[1]}\"")
        if entity is None:
            continue
        base = 'package name'

    if package.startswith('perl-'):
        entity = find_item_from_external_id('P5779', f"\"{package.split('perl-')[1]}\"")
        if entity is None:
            continue
        base = 'package name'

    if package.startswith('python-'):
        entity = find_item_from_external_id('P5568', f"\"{package.split('python-')[1]}\"")
        if entity is not None:
            base = 'package name'

    if package.startswith('rubygem-'):
        entity = find_item_from_external_id('P5566', f"\"{package.split('rubygem-')[1]}\"")
        if entity is not None:
            base = 'package name'

    url = get_website_url(package)
    if url is not None:
        if entity is None:
            entity = find_item_from_external_id('P1324', f"<{url}>") # source code repository
        if entity is None:
            entity = find_item_from_external_id('P2209', f"\"{extract_sourceforge_project(url)}\"")
        if entity is None:
            entity = find_item_from_external_id('P3802', f"\"{extract_launchpad_id(url)}\"")
        if entity is None:
            entity = find_item_from_external_id('P12115', f"\"{extract_savannah_project(url)}\"")
        if entity is None:
            url_variations = generate_url_variations(url)
            if url_variations is None:
                continue
            for url_variation in url_variations:
                entity = find_item_from_external_id('P856', f"<{url_variation}>") # official website
                if entity is not None:
                    break
        if entity is not None and base is None:
            base = 'URL'

    if entity is not None:
        print(f"Found matching Wikidata {entity}")
        claims = entity.get(u'claims')
        if u'P7788' in claims[u'claims']:
            print(u'Skipping: Already has an openSUSE package!')
        else:
            try:
                print(f"Adding openSUSE package")
                claim = pywikibot.Claim(repo, u'P7788')
                claim.setTarget(package)
                entity.addClaim(claim, summary=f'based on {base}.')
            except Exception as error:
                pywikibot.error("Failed add claim for {package}", error)
    print()
