#!/usr/bin/python3

import urllib.request
import re
import pywikibot
import string
import itertools
from pywikibot.data.sparql import SparqlQuery
from bs4 import BeautifulSoup

repo = pywikibot.Site()
repo.login()

class MyAbandonwareEntry():

    def __init__(self, my_abandonware_id):
        url = f"https://www.myabandonware.com/game/--{str(my_abandonware_id)}"
        pywikibot.output(f"Processing URL {url}")
        try:
            request = urllib.request.Request(url, None)
            response = urllib.request.urlopen(request)
            html = response.read().decode("utf-8")
        except Exception as error:
            pywikibot.output(error)
            return

        self.html = html


    def get_external_id(self, label, pattern):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        a = soup.find('a', string=label)
        if a is None:
            return None
        match = re.match(pattern, a['href'])
        if match:
            return match.group(1)
        else:
            return None


def generate_patterns(start=None):
    characters = string.ascii_lowercase + string.digits
    all_patterns = []
    found_start = start is None

    for length in range(1, 4):
        for combo in itertools.product(characters, repeat=length):
            pattern = ''.join(combo)
            if not found_start:
                if pattern == start:
                    found_start = True
                continue
            all_patterns.append(pattern)

    return all_patterns


def find_item_from_external_id(property, id):
    sparql = SparqlQuery()
    try:
        results = sparql.select(f"""
            SELECT ?item WHERE {{
            ?item wdt:{property} "{id}" .
            }}
        """)
    except Exception as error:
        pywikibot.error("Failed to query for {id}", error)
        return None

    if len(results) == 0:
        return None
    match = re.match(r"^https?://www\.wikidata\.org/entity/(Q\d+)$", results[0]["item"])
    if not match:
        return None
    return pywikibot.ItemPage(repo, match.group(1))


def harvest_myabandonware():
    for my_abandonware_id in generate_patterns():
        my_abandonware_page = MyAbandonwareEntry(my_abandonware_id)

        moddb_id = my_abandonware_page.get_external_id('Mod DB', r'^https?:\/\/www\.moddb\.com\/games\/(\w+(?:\-\w+)*)')
        if not moddb_id is None:
            wikidata_game = find_item_from_external_id('P12652', my_abandonware_id)
            if wikidata_game is not None:
                print(f"Found matching Wikidata {wikidata_game}")

                claims = wikidata_game.get(u'claims')
                if u'P6774' in claims[u'claims']:
                    print(u'Skipping: Already has a ModDB game ID!')
                else:
                    moddb_claim = pywikibot.Claim(repo, u'P6774')
                    moddb_claim.setTarget(str(moddb_id))
                    wikidata_game.addClaim(moddb_claim, summary=u'based on MyAbandonware ID.')
                    print(f"Adding ModDB game ID.")

                print()
                continue
            else:
                print(f"No match in Wikidata.")


harvest_myabandonware()