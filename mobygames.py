#!/usr/bin/python3

import csv
from datetime import datetime
import re
import pandas as pd
import pywikibot
import requests
from pywikibot.data.sparql import SparqlQuery
from bs4 import BeautifulSoup

site = pywikibot.Site(url='https://www.c64-wiki.de', fam='c64')

repo = pywikibot.Site()
repo.login()


class MobyGamesEntry():

    def __init__(self, mobygames_id):
        url = f"https://www.mobygames.com/game/{str(mobygames_id)}"
        print(f"Processing URL {url}")
        try:
            response = requests.get(url, headers={"User-Agent": "Wikidata Import"})
            response.raise_for_status()
            html = response.text
        except Exception as error:
            pywikibot.output(error)
            return

        self.html = html

    def get_title(self):
        if not hasattr(self, 'html'):
            return None
        soup = BeautifulSoup(self.html, 'html.parser')
        header = soup.find('h1', class_='mb-0')
        return header.text if header else None

    def get_year(self):
        if not hasattr(self, 'html'):
            return None
        soup = BeautifulSoup(self.html, 'html.parser')
        div_content = soup.find('div', class_='info-release')
        release_date = div_content.find('dt', string="Released").find_next_sibling('dd')
        return extract_year(release_date.find('a').text.strip()) if release_date else None


def extract_year(date_string):
    try:
        date_object = datetime.strptime(date_string, "%B %d, %Y")
        return date_object.year
    except ValueError:
        try:
            date_object = datetime.strptime(date_string, "%Y")
            return date_object.year
        except ValueError:
            return None

class ItchEmbed():

    def __init__(self, itch_id):
        url = f"https://itch.io/embed/{str(itch_id)}"
        print(f"Processing URL {url}")
        try:
            response = requests.get(url, headers={"User-Agent": "Wikidata Import"})
            response.raise_for_status()
            html = response.text
        except Exception as error:
            pywikibot.output(error)
            return

        self.html = html

    def get_title(self):
        if not hasattr(self, 'html'):
            return None
        soup = BeautifulSoup(self.html, 'html.parser')
        header = soup.find('h1', class_='game_title')
        return header.text if header else None

    def get_url(self):
        if not hasattr(self, 'html'):
            return None
        soup = BeautifulSoup(self.html, 'html.parser')
        header = soup.find('h1', class_='game_title')
        return header.find('a')['href'] if header else None


def find_item_from_external_id(property, id):
    sparql = SparqlQuery()
    try:
        results = sparql.select(f"""
            SELECT ?item WHERE {{
            ?item wdt:{property} "{id}" .
            }}
        """)
    except Exception as error:
        pywikibot.error("Failed to query for {id}", error)
        return None

    if results is None:
        return None

    if len(results) == 0:
        return None
    match = re.match(r"^https?://www\.wikidata\.org/entity/(Q\d+)$", results[0]["item"])
    if not match:
        return None
    return pywikibot.ItemPage(repo, match.group(1))


def extract_template_id(source_content):
    match = re.search(r"{{mobygames\|(\d+)}}", source_content)
    if match:
        return match.group(1)
    else:
        return None


def claim_mobygames(wikidata_game, property, id, base):
    claims = wikidata_game.get(u'claims')
    if property in claims[u'claims']:
        print(f'Skipping: Already has a MobyGames ID!')
    else:
        mobygames_claim = pywikibot.Claim(repo, property)
        mobygames_claim.setTarget(str(id))
        wikidata_game.addClaim(mobygames_claim, summary=f'based on {base} ID.')
        print(f"Adding MobyGames ID.")


def harvest_c64wiki():
    category = pywikibot.Category(site, 'Kategorie:Spiel')
    for page in list(category.articles()):
        if page.title() == "C64-Wiki:Musterartikel (Spiel)":
            continue

        if page.title() == "Testartikel (Spielvoting)":
            continue

        if "Übungsartikel" in page.title():
            continue

        print(page.title())

        mobygames_id = extract_template_id(page.text)
        if mobygames_id is None:
            continue

        wikidata_game = find_item_from_external_id('P11688', str(mobygames_id))
        if wikidata_game is not None:
            print(f"Found matching Wikidata {wikidata_game}")
            claim_mobygames(wikidata_game, u'P11688', mobygames_id, 'C64-Wiki')
        else:
            print(f"No match in Wikidata.")

        print()


headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3'
}


class LemonAmigaEntry():

    def __init__(self, lemon_id):
        url = f"https://www.lemonamiga.com/games/details.php?id={str(lemon_id)}"
        print(f"Processing URL {url}")
        try:
            request = urllib.request.Request(url, headers=headers)
            response = urllib.request.urlopen(request)
            html = response.read().decode("utf-8")
        except Exception as error:
            pywikibot.output(error)
            return

        self.html = html

    def get_mobygames_ids(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        a = soup.find('a', title ='External link: More information on MobyGames')
        if a and 'href' in a.attrs:
            return re.search(r"/game/(\d+)/([\w-]+)", a['href'])


def harvest_lemonamiga():
    for lemon_id in range(1, 5008):
        lemon_page = LemonAmigaEntry(lemon_id)
        mobygames = lemon_page.get_mobygames_ids()
        if mobygames is None:
            continue

        mobygames_numeric = mobygames.group(1)
        if mobygames_numeric is not None:
            wikidata_game = find_item_from_external_id('P11688', str(mobygames_numeric))
            if wikidata_game is not None:
                print(f"Found matching Wikidata {wikidata_game}")
                claim_mobygames(wikidata_game, u'P11688', mobygames_numeric, 'Lemon Amiga')
            else:
                print(f"No match in Wikidata.")

        mobygames_slug = mobygames.group(2)
        if mobygames_slug is not None:
            wikidata_game = find_item_from_external_id('P1933', str(mobygames_slug))
            if wikidata_game is not None:
                print(f"Found matching Wikidata {wikidata_game}")
                claim_mobygames(wikidata_game, u'P1933', mobygames_slug, 'Lemon Amiga')
            else:
                print(f"No match in Wikidata.")

        print()


class Lemon64Entry():

    def __init__(self, lemon_id):
        url = f"https://www.lemon64.com/?game_id={str(lemon_id)}"
        print(f"Processing URL {url}")
        try:
            request = urllib.request.Request(url, headers=headers)
            response = urllib.request.urlopen(request)
            html = response.read().decode("utf-8")
        except Exception as error:
            pywikibot.output(error)
            return

        self.html = html

    def get_mobygames_ids(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        a = soup.find('a', title ='External link: More information on MobyGames')
        if a and 'href' in a.attrs:
            return re.search(r"/game/(\d+)/([\w-]+)", a['href'])


def harvest_lemon64():
    for lemon_id in range(1, 7753):
        lemon_page = Lemon64Entry(lemon_id)
        mobygames = lemon_page.get_mobygames_ids()
        if mobygames is None:
            continue

        mobygames_numeric = mobygames.group(1)
        if mobygames_numeric is not None:
            wikidata_game = find_item_from_external_id('P11688', str(mobygames_numeric))
            if wikidata_game is not None:
                print(f"Found matching Wikidata {wikidata_game}")
                claim_mobygames(wikidata_game, u'P11688', mobygames_numeric, 'Lemon64')
            else:
                print(f"No match in Wikidata.")

        mobygames_slug = mobygames.group(2)
        if mobygames_slug is not None:
            wikidata_game = find_item_from_external_id('P1933', str(mobygames_slug))
            if wikidata_game is not None:
                print(f"Found matching Wikidata {wikidata_game}")
                claim_mobygames(wikidata_game, u'P1933', mobygames_slug, 'Lemon64')
            else:
                print(f"No match in Wikidata.")

        print()


def import_stores():

    mobygames_db = pd.read_csv('Mobygames.csv', delimiter=';', dtype=str)

    with open("mobygames.tsv", mode='w', newline='') as file:
        writer = csv.writer(file, delimiter='\t', quotechar="'")

        for index, row in mobygames_db.iterrows():
            print(f"MobyGames ID: " + row['MobyGames'])

            if pd.isna(row['Steam']) and pd.isna(row['itch.io']) and pd.isna(row['GOG slug']):
                print(f"No store identifier.")
                print()
                continue

            wikidata_game = find_item_from_external_id(u'P11688', row['MobyGames'])
            if wikidata_game is not None:
                print(f"Found matching {wikidata_game} based on MobyGames numeric ID")
                print()
                continue

            wikidata_game = find_item_from_external_id(u'P1733', row['Steam'])
            if wikidata_game is not None:
                print(f"Found matching {wikidata_game} based on Steam application ID")
                print()
                continue

            wikidata_game = find_item_from_external_id(u'P12813', row['itch.io'])
            if wikidata_game is not None:
                print(f"Found matching {wikidata_game} based on itch.io numeric ID")
                print()
                continue

            if not pd.isna(row['itch.io']):
                itch_embed = ItchEmbed(row['itch.io'])
                itch_url = itch_embed.get_url()
                wikidata_game = find_item_from_external_id(u'P7294', itch_url)
                if wikidata_game is not None:
                    print(f"Found matching {wikidata_game} based on itch.io URL")
                    print()
                    continue

            wikidata_game = find_item_from_external_id(u'P2725', row['GOG slug'])
            if wikidata_game is not None:
                print(f"Found matching {wikidata_game} based on GOG application ID")
                print()
                continue

            wikidata_game = find_item_from_external_id(u'P12727', row['GOG product'])
            if wikidata_game is not None:
                print(f"Found matching {wikidata_game} based on GOG product ID")
                print()
                continue

            entry = MobyGamesEntry(row['MobyGames'])

            writer.writerow(["CREATE"])
            writer.writerow(["LAST", "Len", f'"{entry.get_title()}"'])
            writer.writerow(["LAST", "Den", f'"{entry.get_year()} video game"'])
            writer.writerow(["LAST", "P31", "Q7889"])

            writer.writerow(["LAST", "P11688", f'"{row["MobyGames"]}"'])

            if not pd.isna(row['Steam']):
                writer.writerow(["LAST", "P1733", f'"{row["Steam"]}"'])

            if not pd.isna(row['itch.io']):
                writer.writerow(["LAST", "P7294", f'"{itch_url}"', "P12813", f'"{row["itch.io"]}"'])

            if not pd.isna(row['GOG slug']):
                writer.writerow(["LAST", "P2725", f'"{row["GOG slug"]}"', "P12727", row['GOG product']])

            file.flush()
            print()


def get_mobygames_numeric_from_gog(mobygames_db, gog_id):
    result = mobygames_db.loc[mobygames_db['GOG slug'] == gog_id, 'MobyGames']
    if not result.empty:
        if not result.size > 1:
            return result.values[0]
        else:
            print("More than one match.")
    else:
        print("GOG ID not found.")


def match_gog_ids():
    mobygames_db = pd.read_csv('Mobygames.csv', delimiter=';', dtype=str)
    for gog_game in mobygames_db['GOG slug']:
        if not pd.isna(gog_game):
            wikidata_game = find_item_from_external_id('P2725', "game/"+gog_game)
            if wikidata_game is not None:
                print(f"Found matching Wikidata {wikidata_game}")
                mobygames_numeric = get_mobygames_numeric_from_gog(mobygames_db, gog_game)
                if mobygames_numeric is not None:
                    claim_mobygames(wikidata_game, u'P11688', mobygames_numeric, 'GOG application')
            else:
                print(f"No match in Wikidata.")


def get_mobygames_numeric_from_steam(mobygames_db, steam_application_id):
    result = mobygames_db.loc[mobygames_db['Steam'] == steam_application_id, 'MobyGames']
    if not result.empty:
        if not result.size > 1:
            return result.values[0]
        else:
            print("More than one match.")
    else:
        print("Steam ID not found.")


def match_steam_ids():
    mobygames_db = pd.read_csv('Mobygames.csv', delimiter=';', dtype=str)
    for steam_application_id in mobygames_db['Steam']:
        if not pd.isna(steam_application_id):
            wikidata_game = find_item_from_external_id('P1733', steam_application_id)
            if wikidata_game is not None:
                print(f"Found matching Wikidata {wikidata_game}")
                mobygames_numeric = get_mobygames_numeric_from_steam(mobygames_db, steam_application_id)
                if mobygames_numeric is not None:
                    claim_mobygames(wikidata_game, u'P11688', mobygames_numeric, 'Steam application')
            else:
                print(f"No match in Wikidata.")


def get_mobygames_numeric_from_itch(mobygames_db, itch_id):
    result = mobygames_db.loc[mobygames_db['itch.io'] == itch_id, 'MobyGames']
    if not result.empty:
        if not result.size > 1:
            return result.values[0]
        else:
            print("More than one match.")
    else:
        print("itch.io ID not found.")


def match_itch_ids():
    mobygames_db = pd.read_csv('Mobygames.csv', delimiter=';', dtype=str)
    for itch_numeric_id in mobygames_db['itch.io']:
        if not pd.isna(itch_numeric_id):
            wikidata_game = find_item_from_external_id('P12813', itch_numeric_id)
            if wikidata_game is not None:
                print(f"Found matching Wikidata {wikidata_game}")
                mobygames_numeric = get_mobygames_numeric_from_itch(mobygames_db, itch_numeric_id)
                if mobygames_numeric is not None:
                    claim_mobygames(wikidata_game, u'P11688', mobygames_numeric, 'itch.io numeric ID')
                    continue

            itch_embed = ItchEmbed(itch_numeric_id)
            itch_url = itch_embed.get_url()

            wikidata_game = find_item_from_external_id('P7294', itch_url)
            if wikidata_game is not None:
                print(f"Found matching Wikidata {wikidata_game}")
                mobygames_numeric = get_mobygames_numeric_from_itch(mobygames_db, itch_numeric_id)
                if mobygames_numeric is not None:
                    claim_mobygames(wikidata_game, u'P11688', mobygames_numeric, 'itch.io URL')
            else:
                print(f"No match in Wikidata.")


#harvest_c64wiki()
#harvest_lemonamiga()
#harvest_lemon64()
#import_stores()
#match_gog_ids()
#match_steam_ids()
match_itch_ids()
