#!/usr/bin/python3

import urllib.request
import re
import pywikibot
from pywikibot.data.sparql import SparqlQuery
from bs4 import BeautifulSoup

repo = pywikibot.Site()
repo.login()


def find_item_from_external_id(property, id):
    sparql = SparqlQuery()
    try:
        results = sparql.select(f"""
            SELECT ?item WHERE {{
            ?item wdt:{property} "{str(id)}" .
            }}
        """)
    except Exception as error:
        pywikibot.error("Failed to query for {id}", error)
        return None

    if results is None:
        return None

    if len(results) == 0:
        return None

    match = re.match(r"^https?://www\.wikidata\.org/entity/(Q\d+)$", results[0]["item"])
    if not match:
        return None

    return pywikibot.ItemPage(repo, match.group(1))


def database_claim(whdload_id, external_property, external_id, identifier_name):
    wikidata_game = find_item_from_external_id(external_property, external_id)
    if wikidata_game is not None:
        print(f"Found matching Wikidata {wikidata_game}")

        claims = wikidata_game.get(u'claims')
        if u'P12225' in claims[u'claims']:
            print(f'Skipping: Already has a WHDLoad database ID!')
        else:
            whdload_claim = pywikibot.Claim(repo, u'P12225')
            whdload_claim.setTarget(str(whdload_id))
            wikidata_game.addClaim(whdload_claim, summary=u'based on ' + identifier_name)
            print(f"Adding WHDLoad database ID")

        print()
        return wikidata_game

    return None


url = "https://www.whdload.de/games/all.html"
try:
    request = urllib.request.Request(url, None)
    response = urllib.request.urlopen(request)
    html = response.read().decode("utf-8")
except Exception as error:
    pywikibot.output(error)

soup = BeautifulSoup(html, 'html.parser')
rows = soup.find('table').find_all('tr')[1:]  # skip the header row

for row in rows:
    columns = row.find_all('td')
    match = re.match(r"^(\S+).html$", columns[1].find('a')['href'])
    if not match:
        continue

    package_name = match.group(1)
    print(f"https://www.whdload.de/games/{package_name}.html")

    hol_ids = [a.text for a in columns[6].find_all('a')]
    lemon_ids = [a.text for a in columns[7].find_all('a')]

    wikidata_game = None
    for hol_id in hol_ids:
        wikidata_game = database_claim(package_name, u'P4671', hol_id, 'Hall of Light ID')
        if wikidata_game is not None:
            continue
    if wikidata_game is None:
        for lemon_id in lemon_ids:
            wikidata_game = database_claim(package_name, u'P4846', lemon_id, 'Lemon Amiga ID')
            if wikidata_game is not None:
                continue

    print()
