#!/usr/bin/env python

from pywikibot.data.sparql import SparqlQuery
import pywikibot
import requests
import string
import re

repo = pywikibot.Site()
repo.login()

def find_item_from_external_id(property, id):
    sparql = SparqlQuery()
    try:
        results = sparql.select(f"""
            SELECT ?item WHERE {{
            ?item wdt:{property} {id} .
            }}
        """)
    except Exception as error:
        pywikibot.error("Failed to query for {id}", error)
        return None

    if len(results) == 0:
        return None
    match = re.match(r"^https?://www\.wikidata\.org/entity/(Q\d+)$", results[0]["item"])
    if not match:
        return None
    return pywikibot.ItemPage(repo, match.group(1))


for letter in string.ascii_lowercase:
    url = f"https://aur.archlinux.org/rpc/v5/search/python-{letter}"
    response = requests.get(url)
    data = response.json()
    results = data.get('results')
    for result in results:
        package = result.get('Name')
        if not package.startswith('python-'):
            continue

        print(package)

        entity = find_item_from_external_id('P5568', f"\"{package.split('python-')[1]}\"")
        if entity is not None:
            print(f"Found matching Wikidata {entity}")
            claims = entity.get(u'claims')
            if u'P4162' in claims[u'claims']:
                print(u'Skipping: Already has an AUR package!')
            else:
                try:
                    print(f"Adding AUR package")
                    claim = pywikibot.Claim(repo, u'P4162')
                    claim.setTarget(package)
                    entity.addClaim(claim, summary=u'based on package name.')
                except Exception as error:
                    pywikibot.error("Failed add claim for {package}", error)
        print()

