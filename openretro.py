#!/usr/bin/python3

import urllib.request
import re
import pywikibot
import requests
import string
from pywikibot.data.sparql import SparqlQuery
from bs4 import BeautifulSoup

repo = pywikibot.Site()
repo.login()


class OpenRetroGame():

    def __init__(self, slug):
        url = f"https://openretro.org{slug}"
        print(f"Processing URL {url}")
        try:
            request = urllib.request.Request(url, None)
            response = urllib.request.urlopen(request)
            html = response.read().decode("utf-8")
        except Exception as error:
            pywikibot.output(error)
            return

        self.html = html

    def get_uuid(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        return soup.find('input', {'name': 'game'})['value']

    def get_lemon(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        link = soup.select_one('.game-button-link-bar a[href*="www.lemonamiga.com"]')
        if link:
            href = link['href']
            return href.split('game_id=')[-1]
        else:
            return None

    def get_moby(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        link = soup.select_one('.game-button-link-bar a[href*="www.mobygames.com"]')
        if link:
            href = link['href']
            return href.split('/')[-1]
        else:
            return None

    def get_wikipedia(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        link = soup.select_one('.game-button-link-bar a[href*="en.wikipedia.org"]')
        if link:
            href = link['href']
            return href.split('/')[-1]
        else:
            return None

    def get_hol(self):
        if not hasattr(self, 'html'):
            return None

        soup = BeautifulSoup(self.html, 'html.parser')
        link = soup.select_one('.game-button-link-bar a[href*="hol.abime.net"]')
        if link:
            href = link['href']
            return href.split('/')[-1]
        else:
            return None


def get_slugs(index):
    url = f"https://openretro.org/browse/amiga/{index}"
    try:
        request = urllib.request.Request(url, None)
        response = urllib.request.urlopen(request)
        html = response.read().decode("utf-8")
    except Exception as error:
        pywikibot.output(error)

    soup = BeautifulSoup(html, 'html.parser')
    game_boxes = soup.find_all('div', class_='game_box')
    for game_box in game_boxes:
        yield game_box.find('a')['href']


def find_item_from_external_id(property, id):
    sparql = SparqlQuery()
    try:
        results = sparql.select(f"""
            SELECT ?item WHERE {{
            ?item wdt:{property} "{id}" .
            }}
        """)
    except Exception as error:
        pywikibot.error("Failed to query for {id}", error)
        return None

    if len(results) == 0:
        return None
    match = re.match(r"^https?://www\.wikidata\.org/entity/(Q\d+)$", results[0]["item"])
    if not match:
        return None
    return pywikibot.ItemPage(repo, match.group(1))


def database_claim(openretro_uuid, property, external_id, identifier_name):
    wikidata_game = find_item_from_external_id(property, external_id)
    if wikidata_game is not None:
        print(f"Found matching Wikidata {wikidata_game}")

        claims = wikidata_game.get(u'claims')
        if u'P7683' in claims[u'claims']:
            print(u'Skipping: Already has an OpenRetro Game Database ID !')
        else:
            claim = pywikibot.Claim(repo, u'P7683')
            claim.setTarget(str(openretro_uuid))
            wikidata_game.addClaim(claim, summary=u'based on ' + identifier_name)
            print(f"Adding OpenRetro Game Database ID.")

        print()
        return wikidata_game

    return None


def get_wikidata_item_from_wikipedia_slug(title):
    url = f"https://en.wikipedia.org/w/api.php"
    params = {
        'action': 'query',
        'prop': 'pageprops',
        'titles': title,
        'format': 'json'
    }

    response = requests.get(url, params=params)
    data = response.json()
    pages = data.get('query', {}).get('pages', {})
    if pages:
        for page_id in pages:
            page = pages[page_id]
            wikibase_item = page.get('pageprops', {}).get('wikibase_item')
            if wikibase_item is not None:
                return pywikibot.ItemPage(repo, wikibase_item)

    return None


def english_wikipedia_claim(openretro_uuid, title):
    wikidata_game = get_wikidata_item_from_wikipedia_slug(title)
    if wikidata_game is not None:
        print(f"Found matching Wikidata {wikidata_game}")

        claims = wikidata_game.get(u'claims')
        if u'P7683' in claims[u'claims']:
            print(u'Skipping: Already has a OpenRetro Game Database ID!')
        else:
            c64wiki_claim = pywikibot.Claim(repo, u'P7683')
            c64wiki_claim.setTarget(openretro_uuid)
            wikidata_game.addClaim(c64wiki_claim, summary=u'based on English Wikipedia title')
            print(f"Adding OpenRetro Game Database ID.")

        print()
        return wikidata_game

    return None


for index in '0' + string.ascii_lowercase:
    urls = get_slugs(index)
    for url in urls:
        game = OpenRetroGame(url)

        wikidata_game = database_claim(game.get_uuid(), 'P4671', game.get_hol(), 'Hall of Light ID.')
        if wikidata_game is not None:
            continue

        wikidata_game = database_claim(game.get_uuid(), 'P4846', game.get_lemon(), 'Lemon Amiga ID.')
        if wikidata_game is not None:
            continue

        wikidata_game = database_claim(game.get_uuid(), 'P1933', game.get_moby(), 'MobyGames game slug.')
        if wikidata_game is not None:
            continue

        wikidata_game = english_wikipedia_claim(game.get_uuid(), game.get_wikipedia())
        if wikidata_game is not None:
            continue

        print()
